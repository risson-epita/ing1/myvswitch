{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  boxfort = stdenv.mkDerivation rec {
    name = "boxfort";

    src = fetchFromGitHub {
      owner = "diacritic";
      repo = "BoxFort";
      rev = "926bd4ce968592dbbba97ec1bb9aeca3edf29b0d";
      sha256 = "0mzy4f8qij6ckn5578y3l4rni2470pdkjy5xww7ak99l1kh3p3v6";
    };

    enableParallelBuilding = true;

    nativeBuildInputs = [ cmake pkgconfig gettext libcsptr dyncall nanomsg ];

    cmakeFlags = [ "-DBXF_TESTS=OFF" "-DBXF_SAMPLES=OFF" "-DBXF_FORK_RESILIENCE=OFF" ];
    doCheck = false;

    meta = with stdenv.lib; {
      description = "Convenient & cross-platform sandboxing C library";
      homepage = "https://github.com/diacritic/BoxFort";
      license = licenses.mit;
      maintainers = [];
        platforms = platforms.unix;
    };
  };
  criterion = stdenv.mkDerivation rec {
    version = "2.3.3";
    pname = "criterion";

    src = fetchFromGitHub {
      owner = "Snaipe";
      repo = "Criterion";
      rev = "v${version}";
      sha256 = "0y1ay8is54k3y82vagdy0jsa3nfkczpvnqfcjm5n9iarayaxaq8p";
      fetchSubmodules = true;
    };

    enableParallelBuilding = true;

    nativeBuildInputs = [ cmake pkgconfig ];

    buildInputs = [
      boxfort
      dyncall
      gettext
      libcsptr
      nanomsg
    ];

    checkInputs = with python37Packages; [
      cram
    ];
    cmakeFlags = [ "-DCTESTS=ON" ];
    doCheck = true;
    preCheck = ''
      export LD_LIBRARY_PATH=`pwd`:$LD_LIBRARY_PATH
      '';
    checkTarget = "criterion_tests test";

    meta = with stdenv.lib; {
      description = "A cross-platform C and C++ unit testing framework for the 21th century";
      homepage = "https://github.com/Snaipe/Criterion";
      license = licenses.mit;
      maintainers = [];
      platforms = platforms.unix;
    };
  };
in mkShell {
  buildInputs = [
    binutils
    gnumake
    gcc9
    criterion
    doxygen
    iperf2
    readline80
    python3
    python37Packages.pyyaml
    python37Packages.termcolor
  ];
}
