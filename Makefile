# Makefile

CC ?= gcc
CPPFLAGS ?= -D_DEFAULT_SOURCE -D_GNU_SOURCE -Isrc
CFLAGS ?= -Wall -Werror -Wextra -std=c18 -pedantic \
					-Wno-address-of-packed-member \
					-Wno-stringop-truncation
LDFLAGS ?= -Wl,--wrap=malloc -Wl,--wrap=calloc -Wl,--wrap=realloc -pedantic
LDLIBS ?= -lreadline

BIN = myvswitch
SRC = vswitch.c fib.c vlan.c \
			frame.c frame_recv.c frame_send.c \
			port.c port_recv.c port_forward.c port_send.c vlan.c \
			xmalloc.c list.c \
			cli.c cli_autocomplete.c cli_builtins.c cli_convert.c cli_helpers.c \
			cmd_analyzer.c cmd_fib.c cmd_interface.c cmd_trace.c cmd_vlan.c
OBJ = ${SRC:.c=.o}

BIN_TEST = test_vswitch
SRC_TEST = 	test_frame_recv.c frame_recv.c \
						test_list.c list.c \
						test_fib.c fib.c \
						xmalloc.c
OBJ_TEST = ${SRC_TEST:.c=.o}

VPATH = src:src/cli:src/cmd:src/fib:src/frame:src/list:src/port:src/vlan:test

all: CPPFLAGS += -DNDEBUG
all: CFLAGS += -O3
all: LDFLAGS += -O3
all: ${BIN}

${BIN}: ${OBJ}
	${CC} ${LDFLAGS} -o $@ $^ ${LDLIBS}

debug: CFLAGS += -g -p
debug: LDFLAGS += -g -p
debug: ${BIN}

debugmem: CFLAGS += -fsanitize=address
debugmem: LDLIBS := -lasan ${LDLIBS}
debugmem: debug

check: ${BIN_TEST}
	./${BIN_TEST} --tap

${BIN_TEST}: LDLIBS += -lcriterion
${BIN_TEST}: ${OBJ_TEST}
	${CC} ${LDFLAGS} -o $@ $^ ${LDLIBS}

distclean:
	${RM} gmon.out* vgcore.*
	${RM} ${OBJ_TEST} ${DEP_TEST}
	${RM} ${OBJ} ${DEP}

clean: distclean
	${RM} ${BIN_TEST}
	${RM} ${BIN}

.PHONY: all check clean debug debugmem distclean

# END
