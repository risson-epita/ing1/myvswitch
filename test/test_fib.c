#include <criterion/criterion.h>

#include "fib/fib.h"

Test(fib, fib_cmp_equal)
{
    struct fib_entry entry =
    {
        .mac = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xab },
    };
    uint8_t mac[ETH_ALEN] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xab };

    int result = fib_cmp(&entry, mac);

    cr_assert_eq(0, result);
}

Test(fib, fib_cmp_not_equal)
{
    struct fib_entry entry =
    {
        .mac = { 0x00, 0x20, 0xb5, 0x67, 0x89, 0xab },
    };
    uint8_t mac[ETH_ALEN] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xab };

    int result = fib_cmp(&entry, mac);

    cr_assert_neq(0, result);
}
