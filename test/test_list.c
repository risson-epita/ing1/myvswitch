#include <criterion/criterion.h>

#include "list/list.h"

struct nb
{
    struct list_item list_item;
    int n;
};

static inline int cmp_int(const void *lhs, const void *rhs)
{
    const struct nb *left = lhs;
    const struct nb *right = rhs;
    if (left->n < right->n)
        return -1;
    if (left->n > right->n)
        return 1;
    return 0;
}

static inline void clear_int(void *element)
{
    (void) element;
    return;
}

static inline struct list *list_init_int(void)
{
    return list_init(cmp_int, clear_int);
}

Test(list, init)
{
    struct list *list = list_init_int();
    cr_assert_eq(0, list_size(list));
    list_destroy(list);
}

Test(list, clear)
{
    struct nb a = { .n = 1 };
    struct nb b = { .n = 2 };
    struct nb c = { .n = 3 };
    struct list *list = list_init_int();
    list_push_front(list, &a);
    list_push_front(list, &b);
    list_push_front(list, &c);

    list_clear(list);

    cr_assert_eq(0, list_size(list));
    list_destroy(list);
}

Test(list, push_front_one_element)
{
    struct nb a = { .n = 5 };
    struct list *list = list_init_int();

    int rc = list_push_front(list, &a);

    cr_assert_eq(1, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_front_several_elements)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct nb c = { .n = 2 };
    struct list *list = list_init_int();

    int rca = list_push_front(list, &a);
    int rcb = list_push_front(list, &b);
    int rcc = list_push_front(list, &c);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rca);
    cr_assert_eq(LIST_SUCCESS, rcb);
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_back_one_element)
{
    struct nb a = { .n = 1 };
    struct list *list = list_init_int();

    int rc = list_push_back(list, &a);

    cr_assert_eq(1, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_back_several_elements)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct nb c = { .n = 2 };
    struct list *list = list_init_int();

    int rca = list_push_back(list, &a);
    int rcb = list_push_back(list, &b);
    int rcc = list_push_back(list, &c);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rca);
    cr_assert_eq(LIST_SUCCESS, rcb);
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_front_back_front)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct nb c = { .n = 2 };
    struct list *list = list_init_int();

    int rca = list_push_front(list, &a);
    int rcb = list_push_back(list, &b);
    int rcc = list_push_front(list, &c);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rca);
    cr_assert_eq(LIST_SUCCESS, rcb);
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_back_front_back)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct nb c = { .n = 2 };
    struct list *list = list_init_int();

    int rca = list_push_back(list, &a);
    int rcb = list_push_front(list, &b);
    int rcc = list_push_back(list, &c);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rca);
    cr_assert_eq(LIST_SUCCESS, rcb);
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, insert_at_zero)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct list *list = list_init_int();
    list_push_front(list, &a);

    int rcb = list_insert_at(list, &b, 0);

    cr_assert_eq(2, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rcb);

    list_clear(list);
    list_destroy(list);
}

Test(list, insert_at_size)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct list *list = list_init_int();
    list_push_front(list, &a);

    int rcb = list_insert_at(list, &b, list_size(list));

    cr_assert_eq(2, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rcb);

    list_clear(list);
    list_destroy(list);
}

Test(list, insert_at_middle_one_element)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct nb c = { .n = 2 };
    struct list *list = list_init_int();
    list_push_front(list, &a);
    list_push_front(list, &b);

    int rcc = list_insert_at(list, &c, 1);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, insert_at_middle_several_elements)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct nb c = { .n = 2 };
    struct nb d = { .n = 21 };
    struct nb e = { .n = 42 };
    struct nb f = { .n = -512 };
    struct list *list = list_init_int();
    list_push_front(list, &a);
    list_push_front(list, &b);

    int rcc = list_insert_at(list, &c, 1);
    int rcd = list_insert_at(list, &d, 1);
    int rce = list_insert_at(list, &e, 1);
    int rcf = list_insert_at(list, &f, 1);

    cr_assert_eq(6, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rcc);
    cr_assert_eq(LIST_SUCCESS, rcd);
    cr_assert_eq(LIST_SUCCESS, rce);
    cr_assert_eq(LIST_SUCCESS, rcf);

    list_clear(list);
    list_destroy(list);
}

Test(list, remove_at_one_element)
{
    struct nb a = { .n = -1 };
    struct list *list = list_init_int();
    list_push_front(list, &a);

    struct nb *ret_a = list_remove_at(list, 0);

    cr_assert_eq(0, list_size(list));
    cr_assert_eq(a.n, ret_a->n);

    list_clear(list);
    list_destroy(list);
}

Test(list, remove_at_several_elements)
{
    struct nb a = { .n = -1 };
    struct nb b = { .n = 0 };
    struct list *list = list_init_int();
    list_push_front(list, &a);
    list_push_front(list, &b);

    struct nb *ret_a = list_remove_at(list, 1);
    struct nb *ret_b = list_remove_at(list, 0);

    cr_assert_eq(0, list_size(list));
    cr_assert_eq(a.n, ret_a->n);
    cr_assert_eq(b.n, ret_b->n);

    list_clear(list);
    list_destroy(list);
}

Test(list, remove_eq)
{
    struct nb a = { .n = 12 };
    struct list *list = list_init_int();
    list_push_front(list, &a);

    struct nb *ret_a = list_remove_eq(list, &a);

    cr_assert_eq(0, list_size(list));
    cr_assert_eq(a.n, ret_a->n);

    list_clear(list);
    list_destroy(list);
}

Test(list, get)
{
    struct nb a = { .n = 5 };
    struct list *list = list_init_int();
    list_push_front(list, &a);

    struct nb *ret_a = list_get(list, 0);

    cr_assert_eq(a.n, ret_a->n);

    list_clear(list);
    list_destroy(list);
}

Test(list, find)
{
    struct nb a = { .n = 42 };
    struct list *list = list_init_int();
    list_push_front(list, &a);

    struct nb *ret_a = list_find(list, &a);

    cr_assert_eq(a.n, ret_a->n);

    list_clear(list);
    list_destroy(list);
}
