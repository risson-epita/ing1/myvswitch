#include <arpa/inet.h>
#include <criterion/criterion.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <stdbool.h>
#include <stdio.h>

#include "frame/frame.h"

static struct msghdr *setup_msg(char *buf, size_t buf_len)
{
    struct msghdr *msg = malloc(sizeof (*msg));
    struct iovec *io = malloc(sizeof (*io));

    io->iov_base = buf;
    io->iov_len = buf_len;
    msg->msg_iov = io;
    msg->msg_iovlen = 1;
    msg->msg_control = NULL;

    return msg;
}

static void free_msg(struct msghdr *msg)
{
    free(msg->msg_control);
    free(msg->msg_iov);
    free(msg);
}

static void add_vlan_in_auxdata(struct msghdr *msg, uint16_t tpid, uint16_t tci)
{
    const size_t cmsg_space = CMSG_SPACE(sizeof (struct tpacket_auxdata));
    msg->msg_control = malloc(cmsg_space);
    msg->msg_controllen = cmsg_space;

    struct tpacket_auxdata aux = { 0 };
    aux.tp_vlan_tci = TCI_VID(tci);
    aux.tp_vlan_tpid = tpid;
    aux.tp_status |= TP_STATUS_VLAN_VALID | TP_STATUS_VLAN_TPID_VALID;

    struct cmsghdr *cmsg = CMSG_FIRSTHDR(msg);
    cmsg->cmsg_level = SOL_PACKET;
    cmsg->cmsg_type = PACKET_AUXDATA;
    cmsg->cmsg_len = CMSG_LEN(sizeof(aux));

    memcpy(CMSG_DATA(cmsg), &aux, sizeof(aux));
}

Test(extract_vlan_tag, no_vlan)
{
    // GIVEN
    struct msghdr *msg = setup_msg(NULL, 0);

    // WHEN
    struct vlan_tag tag;
    bool res = extract_vlan_tag(msg, &tag);

    // THEN
    cr_assert_eq(false, res);

    free_msg(msg);
}

Test(extract_vlan_tag, dot1q_tag)
{
    // GIVEN
    struct msghdr *msg = setup_msg(NULL, 0);
    add_vlan_in_auxdata(msg, ETH_P_8021Q, 0x042);

    // WHEN
    struct vlan_tag tag;
    bool res = extract_vlan_tag(msg, &tag);

    // THEN
    cr_assert_eq(true, res);
    cr_assert_eq(0x042, tag.tci);
    cr_assert_eq(ETH_P_8021Q, tag.tpid);

    free_msg(msg);
}
