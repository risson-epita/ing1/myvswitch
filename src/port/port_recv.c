#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>

#include "fib/fib.h"
#include "frame/frame.h"
#include "list/list.h"
#include "port/port.h"
#include "vswitch.h"

/**
** Receive one frame from a port, and place it in the right output queues.
**
** \param source_port the port to get the frame from.
**
** \param the size of the received frame.
*/
static inline struct frame *port_recv_one_frame(struct port *source_port)
{
    struct frame *frame = malloc(sizeof(struct frame)
                                 + ETH_FRAME_LEN
                                 - sizeof(struct ethhdr));
    ssize_t bytes_read = frame_recv(source_port->sockfd, frame);
    if (bytes_read == -1)
    {
        source_port->is_readable = false;
        free(frame);
        return NULL;
    }

    uint16_t frame_vlan_id = TCI_VID(frame->vlan_tag.tci);
    if (!frame->contains_vlan_tag || frame_vlan_id == 0)
    {
        frame->vlan_tag.tpid = ETH_P_8021Q;
        frame->vlan_tag.tci &= 0xF000;
        frame->vlan_tag.tci |= source_port->native_vlan;
    }
    else
    {
        uint16_t vlan_index = frame_vlan_id / 64;
        if (source_port->native_vlan != frame_vlan_id
            && !((source_port->vlans[vlan_index] >> (frame_vlan_id % 64)) & 1U))
        {
            free(frame);
            return NULL;
        }
    }

    source_port->frames_received += 1;
    source_port->bytes_received += bytes_read;

    return frame;
}

/* Cf. documentation in `port.h`. */
void port_recv(struct vswitch *vswitch, struct port *source_port)
{
    source_port->is_readable = true;
    if (source_port == vswitch->analyzer)
        return;

    while (source_port->is_readable)
    {
        struct frame *frame = port_recv_one_frame(source_port);
        if (frame)
        {
            fib_register(vswitch->vlans[TCI_VID(frame->vlan_tag.tci)].fib,
                                                frame->mac_src, source_port);

            port_forward_frame(vswitch, frame, source_port);

            if (vswitch->tracing)
                frame_print(frame);
        }
    }
}
