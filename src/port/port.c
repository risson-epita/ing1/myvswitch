#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>

#include "cli/cli.h"
#include "fib/fib.h"
#include "frame/frame.h"
#include "list/list.h"
#include "port/port.h"
#include "vswitch.h"

/* Cf. documentation in `port.h`. */
void port_init(struct port *port, int epfd, const char *ifname)
{
    int sockfd, ifindex;
    struct sockaddr_ll sockaddr;

    /* Create the socket. */
    if ((sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1)
        err(EXIT_FAILURE, "failed to create a socket for %s", ifname);

    /* Set the socket as non-blocking, so we can use `epoll` in edge-triggered
     * mode. */
    if (fcntl(sockfd, F_SETFL, O_NONBLOCK) == -1)
        err(EXIT_FAILURE, "failed to set non-blocking socket for %s", ifname);

    /* Set the socket to receive auxiliary data to get the VLAN tag moved by the
     * Kernel */
    int val = 1;
    if (setsockopt(sockfd, SOL_PACKET, PACKET_AUXDATA, &val, sizeof(val)) == -1)
        err(EXIT_FAILURE, "failed to set auxiliary data socket for %s", ifname);

    /* Get the index of the interface from its name. */
    if ((ifindex = if_nametoindex(ifname)) == 0)
        err(EXIT_FAILURE, "failed to find index for %s", ifname);

    /* Bind the socket with the interface. */
    memset(&sockaddr, 0, sizeof(struct sockaddr_ll));
    sockaddr.sll_family = AF_PACKET;
    sockaddr.sll_ifindex = ifindex;
    sockaddr.sll_protocol = htons(ETH_P_ALL);
    if (bind(sockfd, (struct sockaddr *) &sockaddr, sizeof(struct sockaddr_ll))
            == -1)
        err(EXIT_FAILURE, "failed to bind for %s", ifname);

    port->sockfd = sockfd;

    /* Interface name. */
    if (strlen(ifname) >= IF_NAME_LEN)
        warnx("Warning: interface <%s> name truncated", ifname);
    strncpy(port->ifname, ifname, IF_NAME_LEN);

    /* The output queue is represented as a list of frames. */
    port->output_queue = list_init(NULL, frame_free);

    port->is_readable = false;
    port->is_writable = false;

    /* Mirror this port. */
    port->is_mirrored = false;

    /* Native VLAN. */
    port->native_vlan = 1;

    /* Register the socket with epoll. */
    struct epoll_event event =
    {
        .events = EPOLLIN | EPOLLOUT | EPOLLET,
        .data.ptr = port
    };
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, port->sockfd, &event) == -1)
        err(1, "failed to add to epoll for %s", ifname);
}

/* Cf. documentation in `port.h`. */
void port_print_stats(const struct port *port)
{
    printf("    received: %zu bytes in %zu frames\n",
                                port->bytes_received, port->frames_received);
    printf("    sent    : %zu bytes in %zu frames\n",
                                        port->bytes_sent, port->frames_sent);
    if (port->is_mirrored)
        printf("    mirrored\n");
}

/**
** Helper function for the CLI. Used to enumerate all interfaces.
**
** \param cli_interface structure to fill with current interface.
** \param state keeo track of which interfaces we already enumerated.
** \param context global CLI context.
**
** \return `1` if there still are interfaces to enumerate, `0` otherwise.
*/
int cli_enumerate_interfaces(struct cli_interface *cli_interface,
                             void **state,
                             void *context)
{
    struct vswitch *vswitch = context;
    struct port *port;

    if (*state == NULL)
        *state = vswitch->ports;

    port = *state;

    if (port->sockfd)
    {
        cli_interface->id = port->sockfd;
        cli_interface->name = strdup(port->ifname);
        cli_interface->ptr = port;

        *state = port + 1;
        return 1;
    }

    return 0;
}

/* Cf. documentation in `port.h`. */
void port_free(struct port *port)
{
    list_destroy(port->output_queue);
    if (close(port->sockfd) == -1)
        warn("failed to close socket");
}
