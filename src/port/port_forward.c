#include <linux/if_ether.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "fib/fib.h"
#include "frame/frame.h"
#include "list/list.h"
#include "port/port.h"
#include "vswitch.h"

/**
** Whether a MAC address is unicast.
**
** \param mac the MAC address to check.
**
** \return whether the MAC address is unicast.
*/
static inline bool is_unicast(const uint8_t mac[ETH_ALEN])
{
    return mac[0] % 2 == 0;
}

/**
** Add a frame to all ports' output queue, except for the source port and the
** analyzer port.
**
** \param vswitch the virtual switch to manipulate.
** \param frame the frame to broadcast.
** \param source_port the port from which the frame came.
*/
static inline void port_broadcast(struct vswitch *vswitch,
                                  struct frame *frame,
                                  struct port *source_port)
{
    uint16_t frame_vlan_id = TCI_VID(frame->vlan_tag.tci);
    uint16_t vlan_index = frame_vlan_id / 64;
    for (size_t i = 0; i < vswitch->ports_count; ++i)
    {
        struct port *current_port = vswitch->ports + i;
        if (current_port == source_port || current_port == vswitch->analyzer)
            continue;
        if (current_port->native_vlan != frame_vlan_id &&
            !((current_port->vlans[vlan_index] >> (frame_vlan_id % 64)) & 1U))
            continue;
        frame->send_count++;
        list_push_back(current_port->output_queue, frame);
    }
}

/* Cf. documentation in `port.h`. */
void port_forward_frame(struct vswitch *vswitch,
                        struct frame *frame,
                        struct port *source_port)
{
    if (source_port->is_mirrored && vswitch->analyzer)
    {
        frame->send_count++;
        list_push_back(vswitch->analyzer->output_queue, frame);
    }

    if (!is_unicast(frame->mac_dest))
    {
        port_broadcast(vswitch, frame, source_port);
        return;
    }

    struct port *dest_port
        = fib_find(vswitch->vlans[TCI_VID(frame->vlan_tag.tci)].fib,
                   frame->mac_dest);

    if (!dest_port)
    {
        port_broadcast(vswitch, frame, source_port);
        return;
    }

    frame->send_count++;
    list_push_back(dest_port->output_queue, frame);
}
