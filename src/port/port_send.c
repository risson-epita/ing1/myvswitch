#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#include "frame/frame.h"
#include "port/port.h"

/**
** Send one frame from to a port.
**
** \param dest_port the port to send the frame to.
** \param frame the frame to send.
**
** \return the number of bytes sent.
*/
inline ssize_t port_send_one_frame(struct port *dest_port, struct frame *frame)
{
    frame->insert_vlan_tag
                    = TCI_VID(frame->vlan_tag.tci) != dest_port->native_vlan
                    ? true
                    : false;

    ssize_t bytes_sent = frame_send(dest_port->sockfd, frame);
    if (bytes_sent == -1)
    {
        dest_port->is_writable = 0;
        return bytes_sent;
    }

    frame_free(frame);

    dest_port->frames_sent++;
    dest_port->bytes_sent += bytes_sent;

    return bytes_sent;
}

/* Cf. documentation in `port.h`. */
inline void port_send(struct port *dest_port)
{
    if (!dest_port->is_writable || list_size(dest_port->output_queue) == 0)
        return;

    ssize_t bytes_sent = 0;
    struct frame *frame = list_remove_at(dest_port->output_queue, 0);

    while (bytes_sent != -1 && frame)
    {
        if (time(NULL) - frame->received_ts > FRAME_TIMEOUT)
            frame_free(frame);
        else
            bytes_sent = port_send_one_frame(dest_port, frame);

        frame = list_remove_at(dest_port->output_queue, 0);
    }

    if (bytes_sent != -1 && !frame)
        dest_port->is_writable = 1;
}
