#ifndef PORT_H
#define PORT_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "frame/frame.h"
#include "list/list.h"
#include "vswitch.h"

/** Maximum length for an interface name. */
#define IF_NAME_LEN 32

/** Represent a managed interface. */
struct port
{
    /** Socket associated with the interface. */
    int sockfd;
    /** Name of the interface referenced by this structure. */
    char ifname[IF_NAME_LEN];
    /** Array of bytes representing whether a vLAN is tagged on this port. */
    uint64_t vlans[64];
    /** Native/untagged VLAN configured for this port. */
    uint16_t native_vlan;
    /** Queue of waiting to be sent frames. */
    struct list *output_queue;
    /** Whether `recv` returned `EAGAIN` yet. */
    bool is_readable;
    /** Whether `send` returned `EAGAIN` yet. */
    bool is_writable;
    /** Whether the port should be mirrored to the defined analyzer port. */
    bool is_mirrored;
    /** Number of frames sent. */
    size_t frames_sent;
    /** Number of frames received. */
    size_t frames_received;
    /** Number of bytes sent. */
    size_t bytes_sent;
    /** Number of bytes sent. */
    size_t bytes_received;
};

/**
** Initialize a structure port from an interface name, and register it with
** `epoll`.
**
** \param port the structure to initialize.
** \param epfd the `epoll` instance to register to.
** \param ifname the name of the interface to manage.
*/
void port_init(struct port *port, int epfd, const char *ifname);

/**
** Receive frames from a port and add them to the right output queues.
**
** \param vswitch the vswitch to manipulate.
** \param port the port to receive frames from.
*/
void port_recv(struct vswitch *vswitch, struct port *port);

/**
** Place a frame in the right output queue(s).
**
** \param vswitch the virtual switch to manipulate.
** \param frame the frame to manipulate.
** \param source_port the port from which the frame came.
*/
void port_forward_frame(struct vswitch *vswitch,
                        struct frame *frame,
                        struct port *source_port);

/**
** Send frames from a port's output queue to its socket.
**
** \param dest_port the port to send frames to.
*/
void port_send(struct port *dest_port);

/**
** Print the following stats about a port:
** - number of received bytes and frames
** - number of sent bytes and frames
** - whether the port is to be mirrored
**
** \param port the port to print the stats of.
*/
void port_print_stats(const struct port *port);

/**
** Close the socket, empty the output queue, free the port structure.
**
** \param port the port to free.
*/
void port_free(struct port *port);

#endif /* ! PORT_H */
