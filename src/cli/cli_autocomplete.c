#include <stdlib.h>
#include <stdio.h> /* required by readline headers */
#include <string.h>
#include <assert.h>
#include <readline/readline.h>

#include "cli/cli.h"
#include "cli/cli_helpers.h"

#define ARRAY_SIZE(a) (sizeof ((a)) / sizeof(*(a)))

typedef char *(*complete_fcn_t)(const char *word, void **state);

static char *_complete_none(const char *word, void **state);
static char *_complete_command(const char *word, void **state);
static char *_complete_vlan_id(const char *word, void **state);
static char *_complete_vlan_name(const char *word, void **state);
static char *_complete_vlan_name_or_id(const char *word, void **state);
static char *_complete_vlan_range(const char *word, void **state);
static char *_complete_interface_id(const char *word, void **state);
static char *_complete_interface_name(const char *word, void **state);
static char *_complete_interface_name_or_id(const char *word, void **state);
static char *_complete_interface_range(const char *word, void **state);

static complete_fcn_t _complete_fcn_map[] = {
    [CLI_NONE] = &_complete_none,
    [CLI_KEYWORD] = &_complete_none,
    [CLI_COMMAND] = &_complete_command,
    [CLI_STRING] = &_complete_none,
    [CLI_VLAN_ID] = &_complete_vlan_id,
    [CLI_VLAN_NAME] = &_complete_vlan_name,
    [CLI_VLAN_NAME_OR_ID] = &_complete_vlan_name_or_id,
    [CLI_VLAN_RANGE] = &_complete_vlan_range,
    [CLI_INTERFACE_ID] = &_complete_interface_id,
    [CLI_INTERFACE_NAME] = &_complete_interface_name,
    [CLI_INTERFACE_NAME_OR_ID] = &_complete_interface_name_or_id,
    [CLI_INTERFACE_RANGE] = &_complete_interface_range,
};

static bool _cli_valid_range(const char *word)
{
    bool inrange = false;

    if (!*word)
        return false;

    if (*word == '-' || *word == ',')
        return false;

    for (const char *p = word + 1; *p; p++)
    {
        if (*p == ',')
            return _cli_valid_range(p + 1);

        if (*p != '-')
            continue;

        if (inrange)
            return false;

        if (p[1] == ',' || p[1] == '\0')
            return false;

        inrange = true;
    }

    return true;
}

static const struct cli_param *_cli_get_param(const char *line,
                                              const struct cli_command *cmd)
{
    assert(line);
    assert(cmd);

    const struct cli_param *param;
    int offset = 0;

    if (!*line)
        return cmd->params;

    if (strchr(CLI_IFS, line[strlen(line) - 1]))
        /* if line ends with a space, we need to autocomplete with the
         * next parameter */
        offset = 1;

    switch (cli_match(line, cmd->params, &param))
    {
        case CLI_MATCH:
        case CLI_MATCH_PARTIAL:
        case CLI_POTENTIAL_MATCH:
            if (param->type != CLI_NONE)
                return param + offset;
    }

    return NULL;
}

char *cli_autocomplete(const char *word, int rl_state)
{
    assert(word);

    static struct {
        const struct cli_command *cmd;
        const struct cli_param *param;
        void *ptr;
    } state;

    char *line;
    char *str = NULL;

    if (!rl_state)
    {
        state.cmd = cli.commands;
        state.param = NULL;
        state.ptr = NULL;
    }

    while (!state.param || state.param->type == CLI_KEYWORD ||
           state.param->no_completion)
    {
        if (!state.cmd->params)
            return NULL;

        if (!(line = strndup(rl_line_buffer, rl_point)))
            return NULL;

        state.param = _cli_get_param(line, state.cmd++);
        free(line);
        state.ptr = NULL;

        if (!state.param)
            continue;

        if (state.param->no_completion)
            continue;

        if (state.param->type != CLI_KEYWORD)
            break;

        if (strncmp(word, state.param->name, strlen(word)) != 0)
            continue;

        return strdup(state.param->name);
    }

    if (state.param->type == CLI_KEYWORD)
    {
        if (strncmp(word, state.param->name, strlen(word)) != 0)
        {
            state.param = NULL;
            return cli_autocomplete(word, 1);
        }

        str = strdup(state.param->name);
        state.param = NULL;

        return str;
    }

    do {
        free(str);
        assert(state.param->type < ARRAY_SIZE(_complete_fcn_map));
        str = _complete_fcn_map[state.param->type](word, &state.ptr);
    } while (str && strncmp(word, str, strlen(word)) != 0);

    if (!str)
    {
        state.param = NULL;
        return cli_autocomplete(word, 1);
    }

    return str;
}

static char *_complete_none(const char *word, void **state)
{
    (void) word;
    (void) state;

    return NULL;
}

static char *_complete_command(const char *word, void **state)
{
    (void) word;

    struct cli_command *command;

    if (!*state)
        *state = cli.commands;

    command = *state;

    if (!command->params || command->params[0].type == CLI_NONE)
        return NULL;

    *state = command + 1;

    return strdup(command->params[0].name);
}

static char *_complete_vlan_id(const char *word, void **state)
{
    (void) word;

    struct cli_vlan vlan;
    char buf[5];

    while (cli_enumerate_vlans(&vlan, state, cli.context) == 1)
    {
        free(vlan.name);
        if ((size_t ) snprintf(buf, sizeof (buf), "%u", vlan.id) <
            sizeof (buf))
            return strdup(buf);
    }

    return NULL;
}

static char *_complete_vlan_name(const char *word, void **state)
{
    (void) word;

    struct cli_vlan vlan;

    while (cli_enumerate_vlans(&vlan, state, cli.context) == 1)
    {
        if (!vlan.name)
            continue;

        if (*vlan.name)
            return vlan.name;

        free(vlan.name);
    }

    return NULL;
}

static char *_complete_vlan_name_or_id(const char *word, void **state)
{
    (void) word;

    struct _state {
        void *state;
        complete_fcn_t fcn;
    } *_state;
    char *res;

    if (*state == NULL)
    {
        if (!(_state = malloc(sizeof (*_state))))
            return NULL;
        _state->state = NULL;
        _state->fcn = _complete_vlan_id;
        *state = _state;
    } else
        _state = *state;

    if ((res = _state->fcn(word, &_state->state)))
        return res;

    if (_state->fcn == _complete_vlan_id)
    {
        _state->fcn = _complete_vlan_name;
        _state->state = NULL;
        return _complete_vlan_name_or_id(word, state);
    }

    free(_state);

    return NULL;
}

static char *_complete_vlan_range(const char *word, void **state)
{
    char *p = MAX(strrchr(word, ','), strrchr(word, '-'));
    char *str = _complete_vlan_name_or_id(word, state);
    char *newstr;

    if (!p || !str)
        return str;

    if (!(newstr = malloc(strlen(str) + (p - word) + 2)))
        return str;

    strncpy(newstr, word, p - word + 1);
    strcpy(newstr + (p - word) + 1, str);

    if (!_cli_valid_range(newstr))
    {
        free(newstr);
        return str;
    }

    free(str);
    return newstr;
}

static char *_complete_interface_id(const char *word, void **state)
{
    (void) word;

    struct cli_interface interface;
    char buf[5];

    while (cli_enumerate_interfaces(&interface, state, cli.context) == 1)
    {
        free(interface.name);
        if ((size_t ) snprintf(buf, sizeof (buf), "%u", interface.id) <
             sizeof (buf))
            return strdup(buf);
    }

    return NULL;
}

static char *_complete_interface_name(const char *word, void **state)
{
    (void) word;

    struct cli_interface interface;

    while (cli_enumerate_interfaces(&interface, state, cli.context) == 1)
    {
        if (!interface.name)
            continue;

        if (*interface.name)
            return interface.name;

        free(interface.name);
    }

    return NULL;
}

static char *_complete_interface_name_or_id(const char *word, void **state)
{
    (void) word;

    struct _state {
        void *state;
        complete_fcn_t fcn;
    } *_state;
    char *res;

    if (*state == NULL)
    {
        if (!(_state = malloc(sizeof (*_state))))
            return NULL;
        _state->state = NULL;
        _state->fcn = _complete_interface_id;
        *state = _state;
    } else
        _state = *state;

    if ((res = _state->fcn(word, &_state->state)))
        return res;

    if (_state->fcn == _complete_interface_id)
    {
        _state->fcn = _complete_interface_name;
        _state->state = NULL;
        return _complete_interface_name_or_id(word, state);
    }

    free(_state);

    return NULL;
}

static char *_complete_interface_range(const char *word, void **state)
{
    char *p = MAX(strrchr(word, ','), strrchr(word, '-'));
    char *str = _complete_interface_name_or_id(word, state);
    char *newstr;

    if (!p || !str)
        return str;

    if (!(newstr = malloc(strlen(str) + (p - word) + 2)))
        return str;

    strncpy(newstr, word, p - word + 1);
    strcpy(newstr + (p - word) + 1, str);

    if (!_cli_valid_range(newstr))
    {
        free(newstr);
        return str;
    }

    free(str);
    return newstr;
}
