#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <ctype.h>

#include "cli/cli.h"
#include "cli/cli_helpers.h"
#include "cli/cli_convert.h"

int cli_match(const char *line, const struct cli_param *params,
              const struct cli_param **last_param)
{
    char *saveptr;
    char *token;
    char *str;
    int res = CLI_MATCH;

    if (!(str = strdup(line)))
        return CLI_MATCH_ERROR;

    if (last_param)
        *last_param = params;

    for (size_t i = 0; params[i].type != CLI_NONE; i++)
    {
        token = strtok_r(i ? NULL : str, CLI_IFS, &saveptr);

        if (!token)
        {
            free(str);
            return i ? CLI_MATCH_PARTIAL : CLI_MATCH_EMPTY;
        }

        if (last_param)
            *last_param = params + i;

        if (params[i].type == CLI_KEYWORD)
        {
            if (strncmp(token, params[i].name, strlen(token)) != 0)
            {
                free(str);
                return CLI_NO_MATCH;
            }

            if (strlen(token) != strlen(params[i].name))
                res = CLI_POTENTIAL_MATCH;
        }
    }

    if (strtok_r(NULL, " \t", &saveptr))
        res = CLI_NO_MATCH;

    free(str);
    return res;
}

int cli_param_cmp(const struct cli_param *p1, const struct cli_param *p2)
{
    assert(p1);
    assert(p2);

    size_t i;

    for (i = 0; p1[i].type && p2[i].type; i++)
    {
        if (p1[i].type != CLI_KEYWORD && p2[i].type != CLI_KEYWORD)
            continue;

        if (p1[i].type == CLI_KEYWORD)
        {
            size_t l1 = strlen(p1[i].name);
            size_t l2 = strlen(p2[i].name);
            int cmp = strncmp(p1[i].name, p2[i].name, MIN(l1, l2));

            if (cmp != 0)
                return cmp;

            if (l1 < l2)
                return 1;

            if (l1 > l2)
                return -1;
        }
    }

    if (p1[i].type == p2[i].type)
        /* type == CLI_NONE */
        return 0;

    if (!p1[i].type)
        return -1;

    return 1;

}

static void cli_expanded_call(const struct cli_command *cmd, size_t argc,
                              const union cli_opaque_value argv[],
                              size_t index, const struct cli_param *param)
{
    assert(cmd);
    assert(param);
    assert(argc <= CLI_MAX_ARGUMENTS);
    assert(index <= argc);

    if (index == argc || param->type == CLI_NONE)
    {
        cmd->callback(cli.context, cmd->params, argc, argv);
        return;
    }

    if (param->type == CLI_KEYWORD)
    {
        cli_expanded_call(cmd, argc, argv, index, param + 1);
        return;
    }

    if (!param->expand)
    {
        cli_expanded_call(cmd, argc, argv, index + 1, param + 1);
        return;
    }

    union cli_opaque_value values[CLI_MAX_ARGUMENTS];
    memcpy(values, argv, sizeof (*values) * argc);

    for (size_t i = 0; i < argv[index].expanded.count; i++)
    {
        memcpy(values + index, argv[index].expanded.values + i,
               sizeof (*values));
        cli_expanded_call(cmd, argc, values, index + 1, param + 1);
    }
}

void cli_callback(const char *line, const struct cli_command *cmd)
{
    char *saveptr;
    char *token;
    char *str;
    union cli_opaque_value values[CLI_MAX_ARGUMENTS];
    size_t argc;
    size_t i;

    if (!(str = strdup(line)))
    {
        perror("Error");
        return;
    }

    for (i = 0, argc = 0; cmd->params[i].type != CLI_NONE &&
                          argc < CLI_MAX_ARGUMENTS; i++)
    {
        token = strtok_r(i ? NULL : str, " \t", &saveptr);
        if (!token)
        {
            fputs("Error: Incomplete command\n", stderr);
            goto end;
        }

        if (cmd->params[i].type == CLI_KEYWORD)
            continue;

        if (cli_convert(values + argc++, token,
                        cmd->params[i].type) == -1)
        {
            perror("Error");
            argc--;
            goto end;
        }
    }

    memset(values + argc, 0x00, sizeof (*values));
    cli_expanded_call(cmd, argc, values, 0, cmd->params);

end:
    while(i)
        if (cmd->params[--i].type > CLI_KEYWORD)
            cli_convert_free(values + --argc, cmd->params[i].type);
    free(str);
}

int cli_range_strtok(char *str, char **tok1, char **tok2, char **saveptr) {
    assert(tok1);
    assert(tok2);
    assert(saveptr);

    if (str)
        *saveptr = str;

    if (!str && !**saveptr)
        return CLI_RANGE_STOP;

    for (*tok1 = *saveptr; **saveptr && **saveptr != '-'; (*saveptr)++)
        if (**saveptr == ',')
        {
            *tok2 = *tok1;
            *(*saveptr)++ = '\0';
            return CLI_RANGE_CONTINUE;
        }

    if (!**saveptr)
    {
        *tok2 = *tok1;
        return CLI_RANGE_CONTINUE;
    }

    *(*saveptr)++ = '\0';

    for (*tok2 = *saveptr; **saveptr && **saveptr != '-'; (*saveptr)++)
        if (**saveptr == ',')
        {
            *(*saveptr)++ = '\0';
            return CLI_RANGE_CONTINUE;
        }

    if (!**saveptr)
        return CLI_RANGE_CONTINUE;

    errno = EINVAL;
    return CLI_RANGE_ERROR;
}

int cli_get_vlan_by_id(struct cli_vlan *vlan, uint16_t id)
{
    assert(id > 0 && id < 4096);

    int found = 0;
    void *state = NULL;
    struct cli_vlan tmp;

    vlan->id = id;
    vlan->name = NULL;
    vlan->ptr = NULL;

    while (cli_enumerate_vlans(&tmp, &state, cli.context) == 1)
        if (tmp.id == id)
        {
            free(vlan->name);
            *vlan = tmp;
            found = 1;
        } else
            free(tmp.name);

    return found;
}

int cli_get_vlan_by_name(struct cli_vlan *vlan, const char *name)
{
    int found = 0;
    void *state = NULL;
    struct cli_vlan tmp;

    if (!cli_isname(name))
    {
        errno = EINVAL;
        return -1;
    }

    vlan->id = 0;
    if (!(vlan->name = strdup(name)))
        return -1;
    vlan->ptr = NULL;

    while (cli_enumerate_vlans(&tmp, &state, cli.context) == 1)
        if (strcmp(tmp.name, name) == 0)
        {
            free(vlan->name);
            *vlan = tmp;
            found = 1;
        } else
            free(tmp.name);

    return found;
}

int cli_get_interface_by_id(struct cli_interface *interface, uint16_t id)
{
    int found = 0;
    void *state = NULL;
    struct cli_interface tmp;

    interface->id = id;
    interface->name = NULL;
    interface->ptr = NULL;

    while (cli_enumerate_interfaces(&tmp, &state, cli.context) == 1)
        if (tmp.id == id)
        {
            free(interface->name);
            *interface = tmp;
            found = 1;
        } else
            free(tmp.name);

    return found;
}

int cli_get_interface_by_name(struct cli_interface *interface,
                              const char *name)
{
    int found = 0;
    void *state = NULL;
    struct cli_interface tmp;

    if (!cli_isname(name))
    {
        errno = EINVAL;
        return -1;
    }

    interface->id = 0;
    if (!(interface->name = strdup(name)))
        return -1;
    interface->ptr = NULL;

    while (cli_enumerate_interfaces(&tmp, &state, cli.context) == 1)
        if (strcmp(tmp.name, name) == 0)
        {
            free(interface->name);
            *interface = tmp;
            found = 1;
        } else
            free(tmp.name);

    return found;
}

bool cli_isname(const char *name)
{
    assert(name);

    if (!*name)
        return false;

    if (isdigit(*name))
        return false;

    for (const char *p = name; *p; p++)
        if (!isalnum(*p) && *p != '_')
            return false;

    return true;
}
