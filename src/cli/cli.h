#ifndef HEADER_CLI_H
#define HEADER_CLI_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/* This constant contains the default prompt for your CLI. */
#define CLI_PROMPT "myvswitch> "

/*
** The maximum amount of character allowed in a command parameter's name
** (including the trailing '\0').
*/
#define CLI_PARAM_MAX_LEN 32

/*
** The maximum number of arguments that cas be passed by the CLI in a single
** command (excluding keywords).
*/
#define CLI_MAX_ARGUMENTS 16


/*
** This structure is used to tell the CLI about the switch currently defined
** VLANs, the `ptr` field is user defined and should come handy when processing
** commands operating on VLANS
*/
struct cli_vlan {
    uint16_t id;
    char *name;
    void *ptr;
};

/*
** The `cli_interface` structure is fonctionnaly equivalent to the `cli_vlan`
** one, they are splited for sementic reasons: they do not describe the same
** kind of entities.
*/
struct cli_interface {
    uint16_t id;
    char *name;
    void *ptr;
};

/*
** This union is used hold arguments passed-on to the CLI, it is filled before
** calling the user-provided callback, according to a command descriptor.
*/
union cli_opaque_value {
    void *ptr;
    const char *str;
    struct cli_vlan vlan;
    struct cli_interface interface;
    struct {
        size_t count;
        union cli_opaque_value *values;
    } expanded;
};

/*
** This enum lists the type of parameters that can be parsed by the CLI.
*/
enum cli_param_type {
    CLI_NONE = 0,
    CLI_KEYWORD,
    CLI_COMMAND,
    CLI_STRING,
    CLI_VLAN_ID,
    CLI_VLAN_NAME,
    CLI_VLAN_NAME_OR_ID,
    CLI_VLAN_RANGE,
    CLI_INTERFACE_ID,
    CLI_INTERFACE_NAME,
    CLI_INTERFACE_NAME_OR_ID,
    CLI_INTERFACE_RANGE,
};

/*
** This struct describes a command parameter, an array of `struct cli_param`
** termined by an entry of type `CLI_NONE` is a command descriptor.
*/
struct cli_param {
    char name[CLI_PARAM_MAX_LEN];
    enum cli_param_type type;
    bool no_completion;
    bool expand;
};

typedef void (*cli_callback_t)(void *context, const struct cli_param params[],
                               size_t argc,
                               const union cli_opaque_value argv[]);

/*
** A command is described by a command descriptor, a user-provided callback
** and a string to be shown by the `help` command.
*/
struct cli_command {
    const struct cli_param *params;
    cli_callback_t callback;
    const char *help_text;
};

struct cli {
    struct cli_command *commands;
    size_t commands_count;
    bool active;
    void *context;
};

/*
** This is extremely ugly, a proper CLI would not use a global variable to
** hold its state. Unfortunately, readline does not let us do as we would
** since:
**   1. All its own state is global.
**   2. It does not call our line handling function with a pointer to our
**      current state.
**
** Don't do this at home.
*/
extern struct cli cli;

/* CLI initialization.
**
** You must call this function to initialize the CLI global state and readline
** handler functions. It will typically be called once in `main()`.
**
** The context parameter will be passed to your callback functions and should
** help you access your data without having to use global variables. It may be
** NULL.
*/
int cli_init(void *context);

/* CLI command registration.
**
** This function adds a command to the CLI.
*/
int cli_register(const struct cli_param params[], cli_callback_t callback,
                 const char *help_text);

/* CLI command line execution
**
** Execute the given command line. It should mainly be used for writing tests.
**
** Return false when `line` was empty and true otherwise.
**/
int cli_cmdline(const char *line);

/* CLI character handling.
**
** This function instructs readline to read a single character from its input
** stream, typically `stdin`.
**
** It returns `false` when EOF has been read or an exit command has been sent
** by the user, and `true` otherwise. `cli_read()` call `cli_cleanup()` before
** returning `false` to avoid printing an unnecessary prompt.
**
** WARNING: this function may block if you did not configure readline input
** stream to be non-blocking.
*/
bool cli_read(void);

/* CLI printing
**
** This function works like fprintf(3) but will also take care of hiding the
** prompt before printing and will restore it after. It should be useful when
** printing text outside of command handlers.
*/
int cli_fprintf(FILE *stream, const char *format, ...);
#define cli_printf(...) cli_fprintf(stdout, __VA_ARGS__)

/* CLI cleanup function
**
** Remove readline handlers and free the ressources allocated for the CLI
** global state. You may safely call this function multiple times.
*/
void cli_cleanup(void);

/*
** WARNING: YOU MUST PROVIDE THIS FUNCTION!
**
** This function is used by the CLI to match a VLAN name and id to our VLANs
** and to when trying to autocomplete user input. Each successive call must
** return a new VLAN.
**
** The `cli_vlan` parameter is the struct to be filled with the relevant
** values:
**  * The `id` field holds the VLAN ID with (0 < id < 4096).
**  * The `name` field is a pointer to a malloc-ed string or NULL, it will be
**    freed by the CLI.
**  * The `ptr` field is user defined and can be NULL if unused. It is
**    particularly useful in callback to operate on a VLAN argument.
**
** The `state` parameter points to a void pointer, initialy set to NULL, whose
** value is persisted between calls. It can be used to easily iterate over an
** array or a linked-list.
**
** The `context` parameter is set to the value passed to `cli_init()`.
**
** This function is guaranteed to be called once with `*state == NULL` and then
** called again until it returns 1 so you may allocate and free memory if
** needed.
*/
extern int cli_enumerate_interfaces(struct cli_interface *interface,
                                    void **state, void *context);

/*
** WARNING: YOU MUST PROVIDE THIS FUNCTION!
**
** This function is the equivalent of `cli_enumerate_interfaces` for
** interfaces and works the same way.
*/
extern int cli_enumerate_vlans(struct cli_vlan *vlan, void **state,
                               void *context);

#endif /* HEADER_CLI_H */
