#include <stdio.h>
#include <string.h>

#include "cli/cli.h"
#include "cli/cli_builtins.h"

static struct cli_param cmd_help_desc[] = {
    { .name = "help", .type = CLI_KEYWORD },
    { .name = "COMMAND", .type = CLI_COMMAND },
    { .type = CLI_NONE }
};

static struct cli_param cmd_help_all_desc[] = {
    { .name = "help", .type = CLI_KEYWORD, },
    { .type = CLI_NONE }
};

static struct cli_param cmd_exit_desc[] = {
    { .name = "exit", .type = CLI_KEYWORD, },
    { .type = CLI_NONE }
};

static struct cli_param cmd_quit_desc[] = {
    { .name = "quit", .type = CLI_KEYWORD, },
    { .type = CLI_NONE }
};

static void print_cmd_help(const struct cli_command *cmd)
{
    printf("  ");
    for (const struct cli_param *param = cmd->params;
         param->type != CLI_NONE; param++)
    {
        if (param->type == CLI_KEYWORD)
            printf("\033[1m%s\033[0m", param->name);
        else
            printf("\033[4m%s\033[0m", param->name);

        if ((param+1)->type != CLI_NONE)
            printf(" ");
    }

    if (cmd->help_text && *cmd->help_text)
        printf(": \033[3m%s\033[0m\n", cmd->help_text);

}

static void cmd_help(void *context, const struct cli_param params[],
                     size_t argc, const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;

    const char *cmdname = argv[0].str;
    bool found = false;

    for (const struct cli_command *cmd = cli.commands; cmd->params; cmd++)
        if (strcmp(cmd->params[0].name, cmdname) == 0)
        {
            found = true;
            print_cmd_help(cmd);
        }

    if (!found)
        printf("Error: no such command: %s\n", cmdname);
}

static void cmd_help_all(void *context, const struct cli_param params[],
                         size_t argc, const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;
    (void) argv;

    for (const struct cli_command *cmd = cli.commands; cmd->params; cmd++)
        print_cmd_help(cmd);
}

static void cmd_exit(void *context, const struct cli_param params[],
                     size_t argc, const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;
    (void) argv;

    cli.active = false;
}

void cli_builtins_init(void)
{
    cli_register(cmd_help_desc, cmd_help, "Show help for a specific command");
    cli_register(cmd_help_all_desc, cmd_help_all,
                 "Show all availables commands");

    cli_register(cmd_exit_desc, cmd_exit, "Terminate this program");
    cli_register(cmd_quit_desc, cmd_exit, "Terminate this program");
}
