#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#include "cli/cli.h"
#include "cli/cli_helpers.h"
#include "cli/cli_convert.h"

#define ARRAY_SIZE(a) (sizeof ((a)) / sizeof(*(a)))

typedef int (*convert_fcn_t)(union cli_opaque_value *value, const char *word);

static int _convert_none(union cli_opaque_value *value, const char *word);
static int _convert_string(union cli_opaque_value *value, const char *word);
static int _convert_vlan_id(union cli_opaque_value *value, const char *word);
static int _convert_vlan_name(union cli_opaque_value *value, const char *word);
static int _convert_vlan_name_or_id(union cli_opaque_value *value,
                                    const char *word);
static int _convert_vlan_range(union cli_opaque_value *value,
                               const char *word);
static int _convert_interface_id(union cli_opaque_value *value,
                                 const char *word);
static int _convert_interface_name(union cli_opaque_value *value,
                                   const char *word);
static int _convert_interface_name_or_id(union cli_opaque_value *value,
                                         const char *word);
static int _convert_interface_range(union cli_opaque_value *value,
                                    const char *word);

static convert_fcn_t _convert_map[] = {
    [CLI_NONE] = &_convert_none,
    [CLI_KEYWORD] = &_convert_none,
    [CLI_COMMAND] = &_convert_string,
    [CLI_STRING] = &_convert_string,
    [CLI_VLAN_ID] = &_convert_vlan_id,
    [CLI_VLAN_NAME] = &_convert_vlan_name,
    [CLI_VLAN_NAME_OR_ID] = &_convert_vlan_name_or_id,
    [CLI_VLAN_RANGE] = &_convert_vlan_range,
    [CLI_INTERFACE_ID] = &_convert_interface_id,
    [CLI_INTERFACE_NAME] = &_convert_interface_name,
    [CLI_INTERFACE_NAME_OR_ID] = &_convert_interface_name_or_id,
    [CLI_INTERFACE_RANGE] = &_convert_interface_range,
};

typedef void (*convert_free_fcn_t)(union cli_opaque_value *value);

static void _convert_free_none(union cli_opaque_value *value);
static void _convert_free_vlan(union cli_opaque_value *value);
static void _convert_free_vlan_range(union cli_opaque_value *value);
static void _convert_free_interface(union cli_opaque_value *value);
static void _convert_free_interface_range(union cli_opaque_value *value);

static convert_free_fcn_t _convert_free_map[] = {
    [CLI_NONE] = &_convert_free_none,
    [CLI_KEYWORD] = &_convert_free_none,
    [CLI_COMMAND] = &_convert_free_none,
    [CLI_STRING] = &_convert_free_none,
    [CLI_VLAN_ID] = &_convert_free_vlan,
    [CLI_VLAN_NAME] = &_convert_free_vlan,
    [CLI_VLAN_NAME_OR_ID] = &_convert_free_vlan,
    [CLI_VLAN_RANGE] = &_convert_free_vlan_range,
    [CLI_INTERFACE_ID] = &_convert_free_interface,
    [CLI_INTERFACE_NAME] = &_convert_free_interface,
    [CLI_INTERFACE_NAME_OR_ID] = &_convert_free_interface,
    [CLI_INTERFACE_RANGE] = &_convert_free_interface_range,
};

int cli_convert(union cli_opaque_value *value, const char *word,
                enum cli_param_type type)
{
    assert(value);
    assert(word);
    assert(type >= 0 && type < ARRAY_SIZE(_convert_map));

    return _convert_map[type](value, word);
}

static int _convert_none(union cli_opaque_value *value, const char *word)
{
    (void) value;
    (void) word;

    errno = EINVAL;
    return -1;
}

static int _convert_string(union cli_opaque_value *value, const char *word)
{
    value->str = word;

    return 0;
}

static int _convert_vlan_id(union cli_opaque_value *value, const char *word)
{
    long id;
    char *endptr;

    id = strtol(word, &endptr, 0);

    if (*endptr || id <= 0 || id >= 4096)
    {
        errno = EINVAL;
        return -1;
    }

    return cli_get_vlan_by_id(&value->vlan, id);
}

static int _convert_vlan_name(union cli_opaque_value *value, const char *word)
{
    return cli_get_vlan_by_name(&value->vlan, word);
}

static int _convert_vlan_name_or_id(union cli_opaque_value *value,
                                    const char *word)
{
    if (cli_isname(word))
        return _convert_vlan_name(value, word);
    return _convert_vlan_id(value, word);
}

static int _convert_vlan_range(union cli_opaque_value *value, const char *word)
{
    int r;
    char *tok1;
    char *tok2;
    char *saveptr = NULL;
    char *str;

    union cli_opaque_value v1;
    union cli_opaque_value v2;


    if (!(str = strdup(word)))
        return -1;

    value->expanded.count = 0;
    value->expanded.values = NULL;

    while ((r = cli_range_strtok(saveptr ? NULL : str, &tok1, &tok2,
                                 &saveptr)) == CLI_RANGE_CONTINUE)
    {
        if (_convert_vlan_name_or_id(&v1, tok1) == -1)
            goto err;

        if (_convert_vlan_name_or_id(&v2, tok2) == -1)
            goto err_v2;

        if (!v1.vlan.id || !v2.vlan.id)
            goto err_inval;

        if (v1.vlan.id > v2.vlan.id)
        {
            union cli_opaque_value tmp = v1;
            v1 = v2;
            v2 = tmp;
        }

        union cli_opaque_value *v = value->expanded.values;

        for (uint16_t id = v1.vlan.id; id <= v2.vlan.id; id++)
        {
            v = realloc(v, sizeof (*v) * ++value->expanded.count);
            if (!v)
                goto err_realloc;

            struct cli_vlan *tmp = &(v + value->expanded.count - 1)->vlan;
            cli_get_vlan_by_id(tmp, id);
        }

        value->expanded.values = v;

        _convert_free_vlan(&v1);
        _convert_free_vlan(&v2);
    }

    if (r == CLI_RANGE_ERROR)
        goto err;

    free(str);
    return 0;

err_inval:
    errno = EINVAL;
err_realloc:
    _convert_free_vlan(&v2);
err_v2:
    _convert_free_vlan(&v1);
err:
    if (value->expanded.values)
        _convert_free_vlan_range(value);
    free(str);
    return -1;
}

static int _convert_interface_id(union cli_opaque_value *value,
                                 const char *word)
{
    long id;
    char *endptr;

    id = strtol(word, &endptr, 0);

    if (*endptr || id < 0)
    {
        errno = EINVAL;
        return -1;
    }

    return cli_get_interface_by_id(&value->interface, id);
}

static int _convert_interface_name(union cli_opaque_value *value,
                                   const char *word)
{
    return cli_get_interface_by_name(&value->interface, word);
}

static int _convert_interface_name_or_id(union cli_opaque_value *value,
                                    const char *word)
{
    if (cli_isname(word))
        return _convert_interface_name(value, word);
    return _convert_interface_id(value, word);
}

static int _convert_interface_range(union cli_opaque_value *value,
                                    const char *word)
{
    int r;
    char *tok1;
    char *tok2;
    char *saveptr = NULL;
    char *str;

    union cli_opaque_value v1;
    union cli_opaque_value v2;


    if (!(str = strdup(word)))
        return -1;

    value->expanded.count = 0;
    value->expanded.values = NULL;

    while ((r = cli_range_strtok(saveptr ? NULL : str, &tok1, &tok2,
                                 &saveptr)) == CLI_RANGE_CONTINUE)
    {
        errno = EINVAL;
        if (_convert_interface_name_or_id(&v1, tok1) != 1)
            goto err;

        if (_convert_interface_name_or_id(&v2, tok2) != 1)
            goto err_v2;

        if (v1.interface.id > v2.interface.id)
        {
            union cli_opaque_value tmp = v1;
            v1 = v2;
            v2 = tmp;
        }

        union cli_opaque_value *v = value->expanded.values;

        for (uint16_t id = v1.interface.id; id <= v2.interface.id; id++)
        {
            /* We only keep existing interfaces */
            union cli_opaque_value tmp;
            if (cli_get_interface_by_id(&tmp.interface, id) != 1)
            {
                _convert_free_interface(&tmp);
                continue;
            }

            v = realloc(v, sizeof (*v) * ++value->expanded.count);
            if (!v)
                goto err_realloc;

            (v + value->expanded.count - 1)->interface = tmp.interface;
        }

        value->expanded.values = v;

        _convert_free_interface(&v1);
        _convert_free_interface(&v2);
    }

    if (r == CLI_RANGE_ERROR)
        goto err;

    free(str);
    return 0;

err_realloc:
    _convert_free_interface(&v2);
err_v2:
    _convert_free_interface(&v1);
err:
    if (value->expanded.values)
        _convert_free_interface_range(value);
    free(str);
    return -1;
}

void cli_convert_free(union cli_opaque_value *value, enum cli_param_type type)
{
    assert(value);
    assert(type >= 0 && type < ARRAY_SIZE(_convert_free_map));

    _convert_free_map[type](value);
}

static void _convert_free_none(union cli_opaque_value *value)
{
    (void) value;
    assert(value);
}

static void _convert_free_vlan(union cli_opaque_value *value)
{
    assert(value);

    free(value->vlan.name);
}

static void _convert_free_vlan_range(union cli_opaque_value *value)
{
    assert(value);
    assert(value->expanded.values);

    for (size_t i = 0; i < value->expanded.count; i++)
        _convert_free_vlan(value->expanded.values + i);

    free(value->expanded.values);
}

static void _convert_free_interface(union cli_opaque_value *value)
{
    assert(value);

    free(value->interface.name);
}

static void _convert_free_interface_range(union cli_opaque_value *value)
{
    assert(value);
    assert(value->expanded.values);

    for (size_t i = 0; i < value->expanded.count; i++)
        _convert_free_interface(value->expanded.values + i);

    free(value->expanded.values);
}
