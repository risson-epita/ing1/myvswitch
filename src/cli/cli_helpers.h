#ifndef HEADER_CLI_HELPERS_H
#define HEADER_CLI_HELPERS_H

#include "cli/cli.h"

#define CLI_IFS " \t"

#define CLI_MATCH_ERROR -1
#define CLI_NO_MATCH 0
#define CLI_MATCH 1
#define CLI_MATCH_EMPTY 2
#define CLI_MATCH_PARTIAL 3
#define CLI_POTENTIAL_MATCH 4

#define CLI_RANGE_CONTINUE 1
#define CLI_RANGE_STOP 0
#define CLI_RANGE_ERROR -1

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

int cli_match(const char *line, const struct cli_param *params,
              const struct cli_param **param);

int cli_param_cmp(const struct cli_param *p1, const struct cli_param *p2);

void cli_callback(const char *line, const struct cli_command *cmd);

int cli_range_strtok(char *str, char **tok1, char **tok2, char **saveptr);

int cli_get_vlan_by_id(struct cli_vlan *vlan, uint16_t id);
int cli_get_vlan_by_name(struct cli_vlan *vlan, const char *name);

int cli_get_interface_by_id(struct cli_interface *interface, uint16_t id);
int cli_get_interface_by_name(struct cli_interface *interface,
                              const char *name);

bool cli_isname(const char *name);

#endif /* HEADER_CLI_HELPERS_H */
