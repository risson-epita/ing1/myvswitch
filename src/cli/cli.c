#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <ctype.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "cli/cli.h"
#include "cli/cli_helpers.h"
#include "cli/cli_autocomplete.h"
#include "cli/cli_builtins.h"

struct cli cli;

static void _cli_rl_linehandler(char *line)
{
    /* readline got EOF on its input stream */
    if (line == NULL)
    {
        cli_cleanup();
        putchar('\n');
        return;
    }

    if (cli_cmdline(line))
       add_history(line);

    free(line);
}

static char **_cli_rl_complete(const char *text, int start, int end)
{
    (void) start;
    (void) end;

    /* disable readline built-in filename completion */
    rl_attempted_completion_over = 1;

    return rl_completion_matches(text, cli_autocomplete);
}


int cli_init(void *context)
{
    if (!(cli.commands = calloc(1, sizeof (*cli.commands))))
        return -1;
    cli.commands_count = 0;
    cli.context = context;
    cli.active = true;

    rl_callback_handler_install(CLI_PROMPT, _cli_rl_linehandler);
    rl_attempted_completion_function = _cli_rl_complete;

    cli_builtins_init();

    return 0;
}

int cli_register(const struct cli_param params[], cli_callback_t callback,
                 const char *help_text)
{
    assert(params);
    assert(callback);
    assert(params[0].type == CLI_KEYWORD);
    assert(help_text);

    size_t i;
    void *newptr;

    for (i = 0; i < cli.commands_count; i++)
    {
        int cmp = cli_param_cmp(cli.commands[i].params, params);

        if (cmp > 0)
            break;

        if (cmp == 0)
            goto replace_command;
    }

    if (!(newptr = realloc(cli.commands, (cli.commands_count + 2) *
                                         sizeof (*cli.commands))))
        return -1;

    cli.commands = newptr;
    cli.commands_count++;
    memmove(cli.commands + i + 1, cli.commands + i,
            sizeof (*cli.commands) * (cli.commands_count - i));

replace_command:
    cli.commands[i].params = params;
    cli.commands[i].callback = callback;
    cli.commands[i].help_text= help_text;

    return 0;
}

int cli_cmdline(const char *line)
{
    assert(cli.active);

    struct cli_command *last_match = NULL;

    for (struct cli_command *cmd = cli.commands; cmd->params; cmd++)
        switch (cli_match(line, cmd->params, NULL))
        {
            case CLI_MATCH_ERROR:
                perror("Error");
                return true;
            case CLI_MATCH:
                last_match = cmd;
                goto end;
            case CLI_MATCH_EMPTY:
                return false;
            case CLI_MATCH_PARTIAL:
                if (!last_match)
                {
                    fputs("Error: incomplete command\n", stderr);
                    return true;
                }
                break;
            case CLI_POTENTIAL_MATCH:
                if (last_match)
                {
                    fputs("Error: ambiguous command\n", stderr);
                    return true;
                }
                last_match = cmd;
                break;
        }

end:
    if (!last_match)
    {
        fputs("Error: Invalid command\n", stderr);
        return true;
    }

    cli_callback(line, last_match);

    if (!cli.active)
        cli_cleanup();

    return true;
}

bool cli_read(void)
{
    if (!cli.active)
        return false;

    rl_callback_read_char();

    return cli.active;
}

int cli_fprintf(FILE *stream, const char *format, ...)
{
    int size;
    va_list args;

    if (cli.active)
    {
        rl_clear_visible_line();
        fflush(rl_outstream);
    }

    va_start(args, format);
    size = vfprintf(stream, format, args);
    va_end(args);

    if (cli.active)
        rl_forced_update_display();

    return size;
}

void cli_cleanup(void)
{
    rl_callback_handler_remove();
    free(cli.commands);
    cli.commands = NULL;
    cli.commands_count = 0;
    cli.active = false;
}
