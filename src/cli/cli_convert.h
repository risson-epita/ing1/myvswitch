#ifndef HEADER_CLI_CONVERT_H
#define HEADER_CLI_CONVERT_H

#include "cli/cli.h"

int cli_convert(union cli_opaque_value *value, const char *word,
                enum cli_param_type type);

void cli_convert_free(union cli_opaque_value *value, enum cli_param_type type);

#endif /* HEADER_CLI_CONVERT_H */
