#ifndef HEADER_CLI_AUTOCOMPLETE_H
#define HEADER_CLI_AUTOCOMPLETE_H

char *cli_autocomplete(const char *word, int state);

#endif /* HEADER_CLI_AUTOCOMPLETE_H */
