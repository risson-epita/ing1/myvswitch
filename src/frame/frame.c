#include <arpa/inet.h>
#include "inttypes.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "frame/frame.h"

/* Cf. documentation in `frame.h`. */
void frame_print(const struct frame *frame)
{
    printf("src: ");
    for (int i = 0; i < ETH_ALEN - 1; ++i)
        printf("%02X:", frame->mac_src[i]);
    printf("%02X", frame->mac_src[ETH_ALEN - 1]);
    printf(" dest: ");
    for (int i = 0; i < ETH_ALEN - 1; ++i)
        printf("%02X:", frame->mac_dest[i]);
    printf("%02X", frame->mac_dest[ETH_ALEN - 1]);
    printf(" type: ");
    switch (ntohs(frame->ethertype))
    {
        case ETH_P_IP:
            printf("IPv4");
            break;
        case ETH_P_ARP:
            printf("APR");
            break;
        case ETH_P_IPV6:
            printf("IPv6");
            break;
        case 0x88CC:
            printf("LLDP");
            break;
        case ETH_P_8021Q:
            printf("802.1Q");
            break;
        default:
            printf("unknown %04X", ntohs(frame->ethertype));
            break;
    }
    printf("\n");
}

/* Cf. documentation in `frame.h`. */
void frame_free(void *element)
{
    if (!element)
        return;

    struct frame *frame = element;
    frame->send_count--;
    if (frame->send_count == 0)
        free(frame);
}
