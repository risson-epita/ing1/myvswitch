#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#include "frame.h"
#include "port/port.h"

/* Cf. documentation in `frame.h`. */
inline bool extract_vlan_tag(struct msghdr *msg, struct vlan_tag *tag)
{
    struct cmsghdr *cmsg;
    for (cmsg = CMSG_FIRSTHDR(msg); cmsg; cmsg = CMSG_NXTHDR(msg, cmsg))
    {
        // Not a packet auxiliary data, check the next one
        if (cmsg->cmsg_len < CMSG_LEN(sizeof(struct tpacket_auxdata))
            || cmsg->cmsg_level != SOL_PACKET
            || cmsg->cmsg_type != PACKET_AUXDATA)
            continue;

        struct tpacket_auxdata *aux = (void *) CMSG_DATA(cmsg);
        // No VLAN information in this auxiliary data
        if (!(aux->tp_status & TP_STATUS_VLAN_VALID)
            || !(aux->tp_status & TP_STATUS_VLAN_TPID_VALID))
            continue;

        tag->tpid = aux->tp_vlan_tpid;
        tag->tci = aux->tp_vlan_tci;
        return true;
    }
    return false;
}

/* Cf. documentation in `frame.h`. */
ssize_t frame_recv(int source_port_fd, struct frame *frame)
{
    struct sockaddr_ll from;
    struct iovec iov;
    struct msghdr msg;

    /* Alignment trick. */
    union
    {
        struct cmsghdr align;
        char buf[CMSG_SPACE(sizeof(struct tpacket_auxdata))];
    } cmsg_buf;

    iov.iov_base = frame->mac_dest;
    iov.iov_len = ETH_FRAME_LEN;

    msg.msg_name = &from;
    msg.msg_namelen = sizeof(from);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = cmsg_buf.buf;
    msg.msg_controllen = sizeof(cmsg_buf.buf);
    msg.msg_flags = 0;

    errno = 0;
    ssize_t bytes_read = recvmsg(source_port_fd, &msg, 0);
    if (bytes_read == -1)
    {
        if (errno != EAGAIN && errno != EWOULDBLOCK)
            err(EXIT_FAILURE, "failed receiving packet");

        return bytes_read;
    }

    frame->contains_vlan_tag = extract_vlan_tag(&msg, &(frame->vlan_tag));
    frame->insert_vlan_tag = false;

    /* Frame metadata. */
    time(&(frame->received_ts));
    frame->size = bytes_read;
    frame->send_count = 0;

    return bytes_read;
}
