#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "frame/frame.h"

/* Cf. documentation in `frame.h`. */
ssize_t frame_send(int dest_port_fd, struct frame *frame)
{
    char *buf = (void *) frame->mac_dest;

    if (frame->insert_vlan_tag)
    {
        frame->vlan_tag.tpid = htons(frame->vlan_tag.tpid);
        frame->vlan_tag.tci = htons(frame->vlan_tag.tci);
        buf = (void *) &(frame->padding);
        memmove(buf, frame->mac_dest, 2 * ETH_ALEN);
        memcpy(buf + 2 * ETH_ALEN, &(frame->vlan_tag), sizeof(struct vlan_tag));
        frame->size += sizeof(struct vlan_tag);
    }

    errno = 0;
    ssize_t bytes_sent = send(dest_port_fd, buf, frame->size, 0);
    if (bytes_sent == -1 && errno != EAGAIN && errno != EWOULDBLOCK)
        err(EXIT_FAILURE, "failed sending packet");

    if (frame->insert_vlan_tag)
    {
        memmove(frame->mac_dest, buf, sizeof(uint32_t));
        frame->vlan_tag.tpid = ntohs(frame->vlan_tag.tpid);
        frame->vlan_tag.tci = ntohs(frame->vlan_tag.tci);
    }

    return bytes_sent;
}
