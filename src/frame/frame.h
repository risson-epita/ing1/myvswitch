#ifndef FRAME_H
#define FRAME_H

#include <linux/if_ether.h>
#include <linux/types.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "list/list.h"

/** Default timeout before a frame is dropped, in seconds. */
#define FRAME_TIMEOUT 10

/** Extract the VID (VLAN ID) from the TCI. */
#define TCI_VID(tci) ((tci) & 0x0FFF)

/** Extract the QoS bytes (PCP & DEI) from the TCI. */
#define TCI_QOS(tci) ((tci) & 0xF000)

/** Represent a VLAN tag to be stored inside of a frame. */
struct vlan_tag
{
    /** Tag protocol identifier (TPID) bytes. */
    uint16_t tpid;
    /**
     * Tag control information (TCI) bytes. To be used with the macros
     * `TCI_VID` and `TCI_QOS` to get the right bytes.
     */
    uint16_t tci;
} __attribute__((packed));

/** Represent a frame. */
struct frame
{
    /** `struct frame` is destined to be used in an intrusive list. */
    struct list_item list_item;
    /** Size of the frame, header included. */
    ssize_t size;
    /** Number of ports which this frame must still be sent to. */
    size_t send_count;
    /** When the frame was received. */
    time_t received_ts;

    /** The VLAN this frame belongs to. */
    struct vlan_tag vlan_tag;
    /** Whether this frame contained a VLAN tag when it was received. */
    bool contains_vlan_tag;
    /** Whether to insert the VLAN tag in the frame before sending it. */
    bool insert_vlan_tag;

    /** Move the MAC addresses here when inserting the VLAN tag. */
    struct vlan_tag padding;
    /** Destination MAC address. */
    uint8_t mac_dest[ETH_ALEN];
    /** Source MAC address. */
    uint8_t mac_src[ETH_ALEN];
    /** EtherType. */
    __be16 ethertype;
} __attribute__((packed));

/**
** Extract VLAN tag from ancillary data.
**
** If a tag is found, the tag parameter will be filled in host byte order.
**
** Return true if a VLAN tag was found (implying that the tag parameter was
** filled), false otherwise.
*/
bool extract_vlan_tag(struct msghdr *msg, struct vlan_tag *tag);

/**
** Receive a frame from a socket and store it appropriately.
** \todo If the frame contains a VLAN, it is also stored in the frame structure.
**
** \param source_port_fd the socket to receive the frame from.
** \param frame the structure in which to store the frame.
**
** \return the number of bytes read.
*/
ssize_t frame_recv(int source_port_fd, struct frame *frame);

/**
** Send a frame to a socket.
** \todo This also puts the VLAN header at the appropriate place in the buffer
**       before sending it.
**
** \param dest_port_fd the socket to send the frame to.
** \param frame the frame to send.
**
** \return the number of bytes sent.
*/
ssize_t frame_send(int dest_port_fd, struct frame *frame);

/**
** Print the following information about a frame:
** - source MAC address
** - destination MAC address
** - EtherType
** \todo - VLAN information
**
** \param frame the frame to print.
*/
void frame_print(const struct frame *frame);

/**
** Decrement the number of times a frame must still be sent, and free it if it
** reaches zero.
**
** \param element the frame to free.
*/
void frame_free(void *element);

#endif /* ! FRAME_H */
