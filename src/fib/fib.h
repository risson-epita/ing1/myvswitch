#ifndef FIB_H
#define FIB_H

#include <linux/if_ether.h>
#include <stdint.h>

#include "list/list.h"

/** An entry in the Forwarding Information Base. */
struct fib_entry
{
    /** `struct fib_entry` is destined to be used in an intrusive list. */
    struct list_item list_item;
    /**
    ** MAC address of the equipment behind the port, works as the index of
    ** the FIB.
    */
    uint8_t mac[ETH_ALEN];
    /** Port associated with the MAC address. */
    struct port *port;
};

/**
** Compare an entry of the FIB with a MAC address.
** Destined to be used in a list structure, with `list_find`.
**
** \param element an entry in the FIB.
** \param mac_to_find the MAC address to find in the FIB.
**
** \return `LIST_[...]` depending on the output.
*/
int fib_cmp(const void *element, const void *mac_to_find);

/**
** Add or update an entry in the FIB.
** An entry is created if the MAC address isn't yet registered in the FIB.
** If an entry corresponding to the MAC address already exists, it is updated.
**
** \param fib the FIB to modify.
** \param mac the MAC address of the entry to create/update.
** \param port the port to associate with the MAC address.
*/
void fib_register(struct list *fib,
                  uint8_t mac[ETH_ALEN],
                  struct port *port);

/**
** Find a port in an FIB associated with a MAC address.
**
** \param fib the FIB to search.
** \param mac the MAC address to find.
**
** \return if found, the port associated with the MAC address, else `NULL`.
*/
struct port *fib_find(struct list *fib, uint8_t mac[ETH_ALEN]);

#endif /* ! FIB_H */
