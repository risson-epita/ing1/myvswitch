#include <linux/if_ether.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "fib/fib.h"
#include "list/list.h"

/* Cf. documentation in `fib.h`. */
int fib_cmp(const void *element, const void *mac_to_find)
{
    const struct fib_entry *fib_entry = element;
    return memcmp(fib_entry->mac, mac_to_find, ETH_ALEN);
}

/* Cf. documentation in `fib.h`. */
void fib_register(struct list *fib,
                  uint8_t mac[ETH_ALEN],
                  struct port *port)
{
    struct fib_entry *entry = list_find(fib, mac);
    if (!entry)
    {
        entry = malloc(sizeof(struct fib_entry));
        memcpy(entry->mac, mac, ETH_ALEN);
        list_push_back(fib, entry);
    }
    entry->port = port;
}

/* Cf. documentation in `fib.h`. */
struct port *fib_find(struct list *fib, uint8_t mac[ETH_ALEN])
{
    struct fib_entry *entry = list_find(fib, mac);
    if (!entry)
        return NULL;
    return entry->port;
}
