#include <stdlib.h>

#include "list.h"

/* Cf. documentation in `list.h`. */
struct list *list_init(int (*cmp)(const void *, const void *),
                       void (*clear)(void *))
{
    struct list *list = malloc(sizeof(struct list));
    if (!list)
        return NULL;
    list->size = 0;
    list->head = NULL;
    list->tail = NULL;
    list->cmp = cmp;
    list->clear = clear;
    return list;
}

/* Cf. documentation in `list.h`. */
size_t list_size(const struct list *list)
{
    if (!list)
        return -1;
    return list->size;
}

/* Cf. documentation in `list.h`. */
void list_clear(struct list *list)
{
    if (!list)
        return;
    struct list_item *node = list->head;
    while (node)
    {
        struct list_item *tmp = node;
        node = node->next;
        list->clear(tmp);
    }
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
}

/* Cf. documentation in `list.h`. */
void list_destroy(struct list *list)
{
    list_clear(list);
    free(list);
}

/* Cf. documentation in `list.h`. */
void *list_get(const struct list *list, size_t index)
{
    if (!list || index >= list->size)
        return NULL;
    struct list_item *node = list->head;
    for (size_t i = 0; i < index; ++i)
        node = node->next;
    return node;
}

/* Cf. documentation in `list.h`. */
void *list_find(const struct list *list, const void *element)
{
    if (!list)
        return NULL;
    struct list_item *node = list->head;
    for (size_t i = 0; i < list->size; ++i)
    {
        if (!list->cmp(node, element))
            return node;
        node = node->next;
    }
    return NULL;
}

/* Cf. documentation in `list.h`. */
int list_push_front(struct list *list, void *element)
{
    struct list_item *new = element;
    if (!new)
        return LIST_FAIL;
    if (!list->head)
    {
        list->head = new;
        list->tail = new;
        new->prev = NULL;
        new->next = NULL;
    }
    else
    {
        new->next = list->head;
        list->head->prev = new;
        list->head = new;
        new->prev = NULL;
    }
    ++(list->size);
    return LIST_SUCCESS;
}

/* Cf. documentation in `list.h`. */
int list_push_back(struct list *list, void *element)
{
    struct list_item *new = element;
    if (!new)
        return LIST_FAIL;
    if (!list->tail)
    {
        list->head = new;
        list->tail = new;
        new->prev = NULL;
        new->next = NULL;
    }
    else
    {
        new->prev = list->tail;
        list->tail->next = new;
        list->tail = new;
        new->next = NULL;
    }
    ++(list->size);
    return LIST_SUCCESS;
}

/* Cf. documentation in `list.h`. */
int list_insert_at(struct list *list, void *element, size_t index)
{
    struct list_item *new = element;
    if (!list || index > list->size || !new)
        return LIST_FAIL;
    if (index == 0)
        return list_push_front(list, new);
    if (index == list->size)
        return list_push_back(list, new);

    struct list_item *old = list_get(list, index);

    new->prev = old->prev;
    new->prev->next = new;
    old->prev = new;
    new->next = old;

    ++(list->size);
    return LIST_SUCCESS;
}

/* Cf. documentation in `list.h`. */
void *list_remove_at(struct list *list, size_t index)
{
    if (!list || index >= list->size || list->size == 0)
        return NULL;

    struct list_item *to_delete = list_get(list, index);

    if (list->size == 1)
    {
        list->head = NULL;
        list->tail = NULL;
    }
    else if (index == 0)
    {
        list->head = to_delete->next;
        list->head->prev = NULL;
    }
    else if (index == list->size - 1)
    {
        list->tail = to_delete->prev;
        list->tail->next = NULL;
    }
    else
    {
        to_delete->prev->next = to_delete->next;
        to_delete->next->prev = to_delete->prev;
    }

    --(list->size);
    return to_delete;
}

/* Cf. documentation in `list.h`. */
void *list_remove_eq(struct list *list, void *element)
{
    if (!list)
        return 0;
    struct list_item *node = list->head;
    size_t count = 0;
    while (node)
    {
        if (!list->cmp(node, element))
            return list_remove_at(list, count);
        ++count;
        node = node->next;
    }
    return NULL;
}

/* Cf. documentation in `list.h`. */
void list_map(const struct list *list, void (*func)(void *))
{
    if (!list)
        return;
    struct list_item *node = list->head;
    while (node)
    {
        func(node);
        node = node->next;
    }
}
