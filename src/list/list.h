#ifndef LIST_H
#define LIST_H

#include <stddef.h>

/** Return value of some list functions if they succeed. */
#define LIST_SUCCESS 0
/** Return value of some list functions if they fail. */
#define LIST_FAIL -1

/** Represents a single element in a list. */
struct list_item
{
    /** Pointer to the next element in the list. */
    struct list_item *next;
    /** Pointer to the previous element in the list. */
    struct list_item *prev;
};

/**
** Represents a list.
**
** `cmp` awaits a function that has the same behaviour as `strcmp`, but for the
** element stored in the list.
** The first argument in this function will be the data stored in the list.
** The second argument will be whatever you give to the function comparing.
*/
struct list
{
    /** The current size of the list. */
    size_t size;
    /** Pointer to the element at the head of the list. */
    struct list_item *head;
    /** Pointer to the element at the tail of the list. */
    struct list_item *tail;
    /** Function to compare two elements of the list (see notes above). */
    int (*cmp)(const void *, const void *);
    /** Function to execute on an element on the list to clear (free) it. */
    void (*clear)(void *);
};

/**
** Initializes a `list` that can then be used as an argument to all functions
** starting with `list_`.
**
** \param cmp comparison function for two elements stored in the list. Should
**            return `list_LOWER`, `list_EQUAL` or `list_BIGGER`.
** \param clear function used to free an element of the list.
**
** \return a list sentinel to pass to other list functions.
*/
struct list *list_init(int (*cmp)(const void *, const void *),
                       void (*clear)(void *));

/*
** Return the size of a list.
**
** \param list the list of which the size has to be obtained from.
**
** \return the size of the list.
*/
size_t list_size(const struct list *list);

/*
** Delete all elements from a list.
**
** The function `clear`, passed during the initialization of the list is used
** on each element before deleting it.
**
** \param list the list to clear.
*/
void list_clear(struct list *list);

/*
** Clear and destroy a list.
**
** Deletes all elements of a list and then frees the sentinel structure.
**
** \param list the list to destroy.
*/
void list_destroy(struct list *list);

/**
** Add a new element at the beggining of the list.
**
** \param list the list to add the element to.
** \param element the element to add.
**
** \return `LIST_SUCCESS` if the operation succeeded, `LIST_FAIL` otherwise.
*/
int list_push_front(struct list *list, void *element);

/**
** Add a new element at the end of the list.
**
** \param list the list to add the element to.
** \param element the element to add.
**
** \return `LIST_SUCCESS` if the operation succeeded, `LIST_FAIL` otherwise.
*/
int list_push_back(struct list *list, void *element);

/**
** Add a new element at a given index in the list.
**
** \param list the list to add the element to.
** \param element the element to add.
** \param index where to add the element.
**
** \return `LIST_SUCCESS` if the operation succeeded, `LIST_FAIL` otherwise.
*/
int list_insert_at(struct list *list, void *element, size_t index);

/**
** Remove an element at a given index in the list.
**
** \param list the list to remove the element from.
** \param index the index of the element to remove.
**
** \return the deleted element if the operation succeeded, NULL otherwise.
*/
void *list_remove_at(struct list *list, size_t index);

/**
** Remove the first element found in the list.
**
** The elements are compared using the `cmp` function used in the
** initialization.
**
** \param list the list to remove the element from.
** \param element the element to remove from the list, it will be the right-most
**        parameter of the `cmp` function.
**
** \return the deleted element if the operation succeeded, NULL otherwise.
*/
void *list_remove_eq(struct list *list, void *element);

/**
** Find the element at the given index of the list
**
** \param list the list from which we will return the element
** \param index the index of the element
**
** \return the element stored at the given index of the list. NULL is returned
**         if any argument is invalid.
*/
void *list_get(const struct list *list, size_t index);

/**
** Find the index of the first same element in the list.
**
** \param list the list to search in.
** \param element the element to which we'll compare the elements from the list,
**        it will be the right-most parameter of the `cmp` function.
**
** \return the index of the first same element as element in the list. Returns
**         list_FAIL if the element is not found or if any argument is invalid.
*/
void *list_find(const struct list *list, const void *element);

/**
** Apply a function on every element of the list.
**
** \param list the list to modify.
** \param func the function that will be mapped on the list.
*/
void list_map(const struct list *list, void (*func)(void *));

#endif /* ! LIST_H */
