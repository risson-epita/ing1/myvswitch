#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "cli/cli.h"
#include "fib/fib.h"
#include "vlan/vlan.h"
#include "vswitch.h"

int vlan_cmp(const void *lhs, const void *rhs)
{
    const struct vlan *left = lhs;
    const struct vlan *right = rhs;
    return left->id != right->id;
}

void vlan_free(void *element)
{
    if (!element)
        return;
    struct vlan *vlan = element;
    list_destroy(vlan->fib);
    free(vlan);
}

int cli_enumerate_vlans(struct cli_vlan *cli_vlan, void **state, void *context)
{
    struct vswitch *vswitch = context;
    struct vlan *vlan;

    if (*state == NULL)
        *state = vswitch->vlans + 1;

    vlan = *state;

    if (vswitch->vlans + 4096 != vlan)
    {
        cli_vlan->id = vlan->id;
        cli_vlan->name = strdup(vlan->name);
        cli_vlan->ptr = vlan;

        *state = vlan + 1;
        return 1;
    }

    return 0;
}
