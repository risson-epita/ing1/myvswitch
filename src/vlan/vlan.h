#ifndef VLAN_H
#define VLAN_H

#include <stdint.h>

#include "list/list.h"

#define VLAN_NAME_LEN 32

struct vlan
{
    uint16_t id;
    char name[VLAN_NAME_LEN];
    struct list *fib;
};

int vlan_cmp(const void *lhs, const void *rhs);

void vlan_free(void *element);

#endif /* ! VLAN_H */
