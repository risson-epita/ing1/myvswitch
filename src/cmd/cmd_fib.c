#include <inttypes.h>

#include "cli/cli.h"
#include "fib/fib.h"
#include "port/port.h"

static struct cli_param cmd_show_fib_desc[] = {
    { .name = "show", .type = CLI_KEYWORD, },
    { .name = "fib", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

void fib_entry_print(void *element)
{
    const struct fib_entry *entry = element;
    printf("    mac: ");
    for (int i = 0; i < ETH_ALEN - 1; ++i)
        printf("%02X:", entry->mac[i]);
    printf("%02X", entry->mac[ETH_ALEN - 1]);
    printf(" -> port: ");
    printf("%d - %s\n", entry->port->sockfd, entry->port->ifname);
}

static void cmd_show_fib(void *context, const struct cli_param params[],
                         size_t argc, const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;
    (void) argv;

    struct vswitch *vswitch = context;

    for (uint16_t i = 1; i < 4096; ++i)
    {
        if (vswitch->vlans[i].id != i)
            continue;
        printf("FIB for VLAN %u\n", i);
        list_map(vswitch->vlans[i].fib, fib_entry_print);
    }

}

void cmd_fib_init(void)
{
    cli_register(cmd_show_fib_desc, cmd_show_fib, "Print the FIB");
}
