#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "cli/cli.h"
#include "port/port.h"
#include "vlan/vlan.h"

static struct cli_param cmd_interface_show_desc[] = {
    { .name = "show", .type = CLI_KEYWORD, },
    { .name = "interface", .type = CLI_KEYWORD, },
    { .name = "INTERFACES", .type = CLI_INTERFACE_RANGE, .expand = true, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_interface_show_all_desc[] = {
    { .name = "show", .type = CLI_KEYWORD, },
    { .name = "interfaces", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_interface_set_name_desc[] = {
    { .name = "interface", .type = CLI_KEYWORD, },
    { .name = "INTERFACE", .type = CLI_INTERFACE_NAME_OR_ID, },
    { .name = "name", .type = CLI_KEYWORD, },
    { .name = "NEW_NAME", .type = CLI_INTERFACE_NAME,
      .no_completion = true, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_interface_set_mirror_desc[] = {
    { .name = "interface", .type = CLI_KEYWORD, },
    { .name = "INTERFACE", .type = CLI_INTERFACE_NAME_OR_ID, },
    { .name = "mirror", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_interface_unset_mirror_desc[] = {
    { .name = "interface", .type = CLI_KEYWORD, },
    { .name = "INTERFACE", .type = CLI_INTERFACE_NAME_OR_ID, },
    { .name = "no", .type = CLI_KEYWORD, },
    { .name = "mirror", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static void cmd_interface_show(void *context, const struct cli_param params[],
                               size_t argc,
                               const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;
    (void) argv;

    struct vswitch *vswitch = context;
    struct port *port = argv[0].interface.ptr;

    if (!port)
    {
        fputs("Error: Interface not found\n", stderr);
        return;
    }

    printf("  Interface %s\n", port->ifname);
    printf("    id: %d\n", port->sockfd);
    printf("    VLANS: %u(untag)", port->native_vlan);
    for (uint16_t i = 1; i < 4096; ++i)
    {
        if (i == vswitch->vlans[i].id)
        {
            uint16_t vlan_index = i / 64;
            if (!((port->vlans[vlan_index] >> (i % 64)) & 1U))
                continue;
            printf(" %u(tag)", i);
        }
    }
    printf("\n");

    port_print_stats(port);
}

static void cmd_interface_show_all(void *context,
                                   const struct cli_param params[],
                                   size_t argc,
                                   const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;
    (void) argv;

    union cli_opaque_value value[2] = { 0 };

    void *state = NULL;

    while (cli_enumerate_interfaces(&value[0].interface, &state, context) == 1)
    {
        cmd_interface_show(context, params, 1, value);
        free(value[0].interface.name);
    }
}

static void cmd_interface_set_name(void *context,
                                   const struct cli_param params[],
                                   size_t argc,
                                   const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;

    struct port *port = argv[0].interface.ptr;

    if (!port)
    {
        fputs("Error: Interface not found\n", stderr);
        return;
    }

    if (argv[1].interface.ptr && argv[1].interface.ptr != port)
    {
        fputs("Error: interface name already taken\n", stderr);
        return;
    }

    if (strlen(argv[1].interface.name) >= IF_NAME_LEN)
        warnx("Warning: interface name truncated");

    strncpy(port->ifname, argv[1].interface.name, IF_NAME_LEN);
}

static void cmd_interface_set_mirror(void *context,
                                      const struct cli_param params[],
                                      size_t argc,
                                      const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;

    struct port *port = argv[0].interface.ptr;

    if (!port)
    {
        fputs("Error: Interface not found\n", stderr);
        return;
    }

    port->is_mirrored = true;

    puts("Interface set to be analyzed.");
}

static void cmd_interface_unset_mirror(void *context,
                                      const struct cli_param params[],
                                      size_t argc,
                                      const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;

    struct port *port = argv[0].interface.ptr;

    if (!port)
    {
        fputs("Error: Interface not found\n", stderr);
        return;
    }

    port->is_mirrored = false;

    puts("Interface set to not be analyzed.");
}

void cmd_interface_init(void)
{
    cli_register(cmd_interface_show_desc, cmd_interface_show,
                 "Print information about an interface");
    cli_register(cmd_interface_show_all_desc, cmd_interface_show_all,
                 "Print information about all interfaces");
    cli_register(cmd_interface_set_name_desc, cmd_interface_set_name,
                 "Change an interface name");
    cli_register(cmd_interface_set_mirror_desc, cmd_interface_set_mirror,
            "Set an interface to be mirrored to the analyzer port, if set");
    cli_register(cmd_interface_unset_mirror_desc, cmd_interface_unset_mirror,
                 "Set an interface to not be mirrored");
}
