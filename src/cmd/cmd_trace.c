
#include "cli/cli.h"
#include "vswitch.h"

static struct cli_param cmd_trace_on_desc[] = {
    { .name = "trace", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_trace_off_desc[] = {
    { .name = "no", .type = CLI_KEYWORD, },
    { .name = "trace", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static void cmd_trace_on(void *context, const struct cli_param params[],
                         size_t argc, const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;
    (void) argv;

    struct vswitch *vswitch = context;
    vswitch->tracing = true;

    puts("Tracing enabled");
}

static void cmd_trace_off(void *context, const struct cli_param params[],
                          size_t argc, const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;
    (void) argv;

    struct vswitch *vswitch = context;
    vswitch->tracing = false;

    puts("Tracing disabled");
}

void cmd_trace_init(void)
{
    cli_register(cmd_trace_on_desc, cmd_trace_on, "Enable frame tracing");
    cli_register(cmd_trace_off_desc, cmd_trace_off,
                 "Disable frame tracing");
}
