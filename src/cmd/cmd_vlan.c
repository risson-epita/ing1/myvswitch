#include <err.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cli/cli.h"
#include "cmd/cmd_vlan.h"
#include "port/port.h"
#include "vlan/vlan.h"
#include "vswitch.h"

static struct cli_param cmd_vlan_show_desc[] = {
    { .name = "show", .type = CLI_KEYWORD, },
    { .name = "vlan", .type = CLI_KEYWORD, },
    { .name = "VLANS", .type = CLI_VLAN_RANGE, .expand = true, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_vlan_show_all_desc[] = {
    { .name = "show", .type = CLI_KEYWORD, },
    { .name = "vlans", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_vlan_set_name_desc[] = {
    { .name = "vlan", .type = CLI_KEYWORD, },
    { .name = "VLAN", .type = CLI_VLAN_NAME_OR_ID, },
    { .name = "name", .type = CLI_KEYWORD, },
    { .name = "NEW_NAME", .type = CLI_VLAN_NAME, .no_completion = true, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_vlan_unset_name_desc[] = {
    { .name = "no", .type = CLI_KEYWORD, },
    { .name = "vlan", .type = CLI_KEYWORD, },
    { .name = "VLANS", .type = CLI_VLAN_RANGE, .expand = true, },
    { .name = "name", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_vlan_set_tag_desc[] = {
    { .name = "interface", .type = CLI_KEYWORD, },
    { .name = "INTERFACES", .type = CLI_INTERFACE_RANGE, .expand = true },
    { .name = "vlan", .type = CLI_KEYWORD, },
    { .name = "VLANS", .type = CLI_VLAN_RANGE, .expand = true, },
    { .name = "tag", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_vlan_set_untag_desc[] = {
    { .name = "interface", .type = CLI_KEYWORD, },
    { .name = "INTERFACES", .type = CLI_INTERFACE_RANGE, .expand = true },
    { .name = "vlan", .type = CLI_KEYWORD, },
    { .name = "VLAN", .type = CLI_VLAN_NAME_OR_ID, },
    { .name = "untag", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_vlan_unset_desc[] = {
    { .name = "no", .type = CLI_KEYWORD, },
    { .name = "interface", .type = CLI_KEYWORD, },
    { .name = "INTERFACES", .type = CLI_INTERFACE_RANGE, .expand = true },
    { .name = "vlan", .type = CLI_KEYWORD, },
    { .name = "VLANS", .type = CLI_VLAN_RANGE, .expand = true, },
    { .type = CLI_NONE },
};


static void cmd_vlan_show(void *context, const struct cli_param params[],
                          size_t argc, const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;
    (void) argv;

    struct vswitch *vswitch = context;
    struct vlan *vlan = argv[0].vlan.ptr;

    if (!vlan)
    {
        warnx("Error: VLAN not found");
        return;
    }

    printf("  VLAN %u\n", vlan->id);
    if (*(vlan->name))
        printf("    name: %s\n", vlan->name);
    else
        printf("    name: (not set)\n");

    if (vlan == vswitch->native_vlan)
        printf("    this is the native VLAN\n");

    printf("    interfaces:");

    size_t vlan_index = vlan->id / 64;
    for (size_t i = 0; i < vswitch->ports_count; ++i)
    {
        struct port *port = vswitch->ports + i;

        if ((port->vlans[vlan_index] >> (vlan->id % 64)) & 1U)
            printf(" %d(tag)", port->sockfd);

        if (port->native_vlan == vlan->id)
            printf(" %d(untag)", port->sockfd);
    }
    printf("\n");
}

static void cmd_vlan_show_all(void *context, const struct cli_param params[],
                              size_t argc, const union cli_opaque_value argv[])
{
    (void) argc;
    (void) argv;

    union cli_opaque_value value[2] = {
        { .ptr = NULL },
        { .ptr = NULL },
    };

    void *state = NULL;

    while (cli_enumerate_vlans(&value[0].vlan, &state, context) == 1)
    {
        cmd_vlan_show(context, params, 1, value);
        free(value[0].vlan.name);
    }
}

static void cmd_vlan_set_name(void *context, const struct cli_param params[],
                              size_t argc, const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;

    struct vlan *vlan = argv[0].vlan.ptr;

    if (!vlan)
    {
        warnx("Error: VLAN not found");
        return;
    }

    if (argv[1].vlan.ptr && argv[1].vlan.ptr != vlan)
    {
        warnx("Error: interface name already taken");
        return;
    }

    if (strlen(argv[1].vlan.name) >= VLAN_NAME_LEN)
        warnx("Warning: interface name truncated");

    strncpy(vlan->name, argv[1].vlan.name, VLAN_NAME_LEN);
}

static void cmd_vlan_set_tag(void *context, const struct cli_param params[],
                             size_t argc, const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;

    struct port *port = argv[0].vlan.ptr;
    struct vlan *vlan = argv[1].vlan.ptr;

    if (!port)
    {
        warnx("Error: Interface not found");
        return;
    }

    if (!vlan)
    {
        warnx("Error: VLAN not found");
        return;
    }

    if (vlan->id == port->native_vlan)
    {
        warnx("Error: VLAN is already set as untag on this port");
        return;
    }

    uint16_t vlan_index = vlan->id / 64;
    port->vlans[vlan_index] |= (1ULL << (vlan->id % 64));
    list_clear(vlan->fib);

    puts("VLAN successfully set.");
}

static void cmd_vlan_set_untag(void *context, const struct cli_param params[],
                               size_t argc,
                               const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;

    struct port *port = argv[0].interface.ptr;
    struct vlan *vlan = argv[1].vlan.ptr;

    if (!port)
    {
        warnx("Error: Interface not found");
        return;
    }

    if (!vlan)
    {
        warnx("Error: VLAN not found");
        return;
    }

    port->native_vlan = vlan->id;
    list_clear(vlan->fib);

    puts("VLAN successfully set.");
}

static void cmd_vlan_unset(void *context, const struct cli_param params[],
                           size_t argc, const union cli_opaque_value argv[])
{
    (void) context;
    (void) params;
    (void) argc;

    struct port *port = argv[0].interface.ptr;
    struct vlan *vlan = argv[1].vlan.ptr;

    if (!port)
    {
        warnx("Error: Interface not found");
        return;
    }

    if (!vlan)
    {
        warnx("Error: VLAN not found");
        return;
    }

    size_t vlan_index = vlan->id / 64;
    port->vlans[vlan_index] &= ~(1ULL << (vlan->id % 64));
    list_clear(vlan->fib);

    puts("VLAN successfully unset.");
}

void cmd_vlan_init(void)
{
    cli_register(cmd_vlan_show_desc, cmd_vlan_show,
                 "Print information about a VLAN");
    cli_register(cmd_vlan_show_all_desc, cmd_vlan_show_all,
                 "Print information about all VLANs");
    cli_register(cmd_vlan_set_name_desc, cmd_vlan_set_name,
                 "Assign a name to a VLAN");
    cli_register(cmd_vlan_unset_name_desc, cmd_vlan_set_name,
                 "Unset a VLAN name");

    cli_register(cmd_vlan_set_tag_desc, cmd_vlan_set_tag,
                 "Assign a VLAN as tagged on an interface");
    cli_register(cmd_vlan_set_untag_desc, cmd_vlan_set_untag,
                 "Select the untagged VLAN for an interface");
    cli_register(cmd_vlan_unset_desc, cmd_vlan_unset,
                 "Remove a tagged VLAN from an interface");
}
