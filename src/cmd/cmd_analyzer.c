#include "cli/cli.h"
#include "port/port.h"
#include "vswitch.h"

static struct cli_param cmd_analyzer_set_desc[] = {
    { .name = "analyzer", .type = CLI_KEYWORD, },
    { .name = "INTERFACE", .type = CLI_INTERFACE_NAME_OR_ID, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_analyzer_unset_desc[] = {
    { .name = "no", .type = CLI_KEYWORD, },
    { .name = "analyzer", .type = CLI_KEYWORD, },
    { .name = "INTERFACE", .type = CLI_INTERFACE_NAME_OR_ID, },
    { .type = CLI_NONE },
};

static struct cli_param cmd_analyzer_show_desc[] = {
    { .name = "show", .type = CLI_KEYWORD, },
    { .name = "analyzer", .type = CLI_KEYWORD, },
    { .type = CLI_NONE },
};

static void cmd_analyzer_set(void *context,
                             const struct cli_param params[],
                             size_t argc,
                             const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;

    struct vswitch *vswitch = context;

    struct port *port = argv[0].interface.ptr;

    if (!port)
    {
        fputs("Error: Interface not found\n", stderr);
        return;
    }

    vswitch->analyzer = port;

    puts("Interface analyzer set");
}

static void cmd_analyzer_unset(void *context,
                               const struct cli_param params[],
                               size_t argc,
                               const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;
    (void) argv;

    struct vswitch *vswitch = context;

    vswitch->analyzer = NULL;

    puts("Interface analyzer unset");
}

static void cmd_analyzer_show(void *context,
                              const struct cli_param params[],
                              size_t argc,
                              const union cli_opaque_value argv[])
{
    (void) params;
    (void) argc;
    (void) argv;

    struct vswitch *vswitch = context;

    if (!vswitch->analyzer)
    {
        puts("No analyzer interface set");
        return;
    }

    printf("  Interface %s\n", vswitch->analyzer->ifname);
    printf("    id: %u\n", vswitch->analyzer->sockfd);
    port_print_stats(vswitch->analyzer);
}

void cmd_analyzer_init(void)
{
    cli_register(cmd_analyzer_set_desc, cmd_analyzer_set,
                 "Set an interface as an analyzer");
    cli_register(cmd_analyzer_unset_desc, cmd_analyzer_unset,
                 "Disable interface analyzer");
    cli_register(cmd_analyzer_show_desc, cmd_analyzer_show,
                 "Show information about the analyzer interface");
}
