#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <unistd.h>

#include "cli/cli.h"
#include "cmd/cmd_analyzer.h"
#include "cmd/cmd_fib.h"
#include "cmd/cmd_interface.h"
#include "cmd/cmd_trace.h"
#include "cmd/cmd_vlan.h"
#include "fib/fib.h"
#include "port/port.h"
#include "vlan/vlan.h"
#include "vswitch.h"

static inline struct vswitch *switch_init(int argc, char *argv[])
{
    struct vswitch *vswitch = calloc(1, sizeof(struct vswitch));

    vswitch->tracing = false;
    vswitch->analyzer = NULL;

    vswitch->epfd = epoll_create(argc);
    if (vswitch->epfd == -1)
        err(EXIT_FAILURE, "failed to create epoll instance");

    vswitch->ports_count = argc;
    vswitch->ports = calloc((argc + 1), sizeof(struct port));
    for (int i = 0; i < argc; ++i)
        port_init(vswitch->ports + i, vswitch->epfd, argv[i]);

    for (uint16_t i = 1; i < 4096; ++i)
    {
        vswitch->vlans[i].id = i;
        vswitch->vlans[i].fib = list_init(fib_cmp, free);
    }
    strncpy(vswitch->vlans[1].name, "DEFAULT_VLAN", VLAN_NAME_LEN);

    struct epoll_event event =
    {
        .events = EPOLLIN,
        .data.ptr = NULL
    };
    if (epoll_ctl(vswitch->epfd, EPOLL_CTL_ADD, STDIN_FILENO, &event) == -1)
        err(EXIT_FAILURE, "failed to add to epoll for the cli");

    cli_init(vswitch);

    cmd_analyzer_init();
    cmd_fib_init();
    cmd_interface_init();
    cmd_trace_init();
    cmd_vlan_init();

    return vswitch;
}

static inline bool epoll_handle_events(struct vswitch *vswitch)
{
    struct epoll_event evlist[EPOLL_MAX_EVENTS];
    int event_count = epoll_wait(vswitch->epfd, evlist, EPOLL_MAX_EVENTS, -1);
    if (event_count == -1)
        err(EXIT_FAILURE, "failed to get events from epoll");

    for (int i = 0; i < event_count; ++i)
    {
        struct epoll_event *event = evlist + i;
        struct port *port = event->data.ptr;

        if (event->events & EPOLLIN)
        {
            // event.data.ptr is set to NULL when add the CLI to epoll
            if (port)
                port_recv(vswitch, port);
            else
            {
                if (cli_read())
                    continue;
                else
                    return false;
            }
        }

        if (event->events & EPOLLOUT)
        {
            port->is_writable = true;
            port_send(port);
        }
    }
    return true;
}

static inline void switch_free(struct vswitch *vswitch)
{
    if (close(vswitch->epfd) == -1)
        warn("failed to close epoll instance");
    for (uint16_t i = 1; i < 4096; ++i)
        list_destroy(vswitch->vlans[i].fib);
    for (size_t i = 0; i < vswitch->ports_count; ++i)
        port_free(vswitch->ports + i);
    free(vswitch->ports);
    cli_cleanup();
}

int main(int argc, char *argv[])
{
    if (argc == 1)
        errx(EXIT_FAILURE, "usage: %s IF1 [IF2...]", argv[0]);

    struct vswitch *vswitch = switch_init(argc - 1, argv + 1);

    while (epoll_handle_events(vswitch))
    {
        for (size_t i = 0; i < vswitch->ports_count; ++i)
            port_send(vswitch->ports + i);
    }

    switch_free(vswitch);
    return 0;
}
