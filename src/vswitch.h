#ifndef SWITCH_H
#define SWITCH_H

#include <stddef.h>

#define EPOLL_MAX_EVENTS 10

#include "vlan/vlan.h"

struct vswitch
{
    int epfd;
    struct port *ports;
    size_t ports_count;
    bool tracing;
    struct port *analyzer;
    struct vlan vlans[4096];
    struct vlan *native_vlan;
};

#endif /* ! SWITCH_H */
