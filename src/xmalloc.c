#include <err.h>
#include <stdlib.h>

void *__real_malloc(size_t size);
void *__real_calloc(size_t nmemb, size_t size);
void *__real_realloc(void *ptr, size_t size);

void *__wrap_malloc(size_t size)
{
    void *p = __real_malloc(size);
    if (!p)
        err(EXIT_FAILURE, "failed to malloc %zu bytes", size);
    return p;
}

void *__wrap_calloc(size_t nmemb, size_t size)
{
    void *p = __real_calloc(nmemb, size);
    if (!p)
        err(EXIT_FAILURE, "failed to calloc %zu element of %zu bytes", nmemb,
                                                                       size);
    return p;
}

void *__wrap_realloc(void *ptr, size_t size)
{
    void *p = __real_realloc(ptr, size);
    if (!p)
        err(EXIT_FAILURE, "failed to realloc %p to %zu bytes", ptr, size);
    return p;
}
